# Wifi Access Point
TP-Link Archer C7 vom Router zum Access Point umwandeln.

## Netzwerkverbindung aufbauen
---
- Router per Taste ganz rechts hinten resetten
- Dann die IP und Default Gateway Ip der Netzwerkverbindung manuell setzen, dass
  - die Ip in der Range des Routers ist, aber nicht identisch (hier: `192.168.0.10`)
  - die Subnet Mask normal `255.255.255.0`
  - die Default Gateway Ip, die Ip vom Router ist (`192.168.0.1`)
- Dann in `cmd.exe` den *DNS* flushen mittels `ipconfig /flushdns`
- Dann im Browser `192.168.0.1` öffnen und per Benutzername `admin` und Passwort `admin` einloggen.