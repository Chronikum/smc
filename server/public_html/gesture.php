<?php
//mysql connection
$mysqli = new mysqli("localhost", "smc_user", "password", "smc");
if ($mysqli->connect_errno) {
    die("Verbindung fehlgeschlagen: " . $mysqli->connect_error);
}
$output_data = array("success"=>"true");
$outputContent = json_encode($output_data);
closeOutput( $outputContent );

$gesture = $_GET['gesture'];
$device = $_GET['device'];
$gesture = $mysqli->real_escape_string($gesture);
$device = $mysqli->real_escape_string($device);

$query = "SELECT * FROM `actions` WHERE id=(SELECT id FROM linkings WHERE gesture_id=$gesture AND device_id=$device);";
$result = $mysqli->query($query);
$counter = 0;
$done = "";
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
		$counter++;
		$gpio_id = $row['gpio_id'];
		$http_id = $row['http_id'];
		if($gpio_id>-1) {
			$query2 = "SELECT * FROM `gpio` WHERE id=$gpio_id;";
			$result2 = $mysqli->query($query2);
			$row2 = $result2->fetch_assoc();
			$pin = $row2['pin'];
			$state1 = $row2['state1'];
			$delay = $row2['delay'];
			$state2 = $row2['state2'];
			exec("gpio mode $pin out");
			exec("gpio write $pin $state1");
			usleep(1000*$delay);
			exec("gpio write $pin $state2");
			$done .= "Pin $pin auf $state;";
		}
		if($http_id>-1) {
			//TODO: add http logic
		}
    }
}
else {
	// no record found in DB
}
$mysqli->close();
function closeOutput($stringToOutput){   
        set_time_limit(0);
        ignore_user_abort(true);
        header("Connection: close\r\n");
        header("Content-Encoding: none\r\n");  
        ob_start();          
        echo $stringToOutput;   
        $size = ob_get_length();   
        header("Content-Length: $size",TRUE);  
        ob_end_flush();
        ob_flush();
        flush();   
} 
?>