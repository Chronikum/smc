<?php
//mysql connection
$mysqli = new mysqli("localhost", "smc_user", "password", "smc");
if ($mysqli->connect_errno) {
    die("Verbindung fehlgeschlagen: " . $mysqli->connect_error);
}

$query = "SELECT * FROM `gestures`;";
$result = $mysqli->query($query);
$gestures = [];
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
		$gestures[]= $row;
	}
}
$output_data = array("success"=>"true","gestures"=>$gestures);
echo json_encode($output_data);
$mysqli->close();
?>