<?php
//mysql connection
$mysqli = new mysqli("localhost", "smc_user", "password", "smc");
if ($mysqli->connect_errno) {
    die("Verbindung fehlgeschlagen: " . $mysqli->connect_error);
}

$query = "SELECT * FROM `devices`;";
$result = $mysqli->query($query);
$devices = [];
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
		$devices[]= $row;
	}
}
$output_data = array("success"=>"true","devices"=>$devices);
echo json_encode($output_data);
$mysqli->close();
?>