<?php
//mysql connection...
$mysqli = new mysqli("localhost", "user", "password", "database");
if ($mysqli->connect_errno) {
    die("Verbindung fehlgeschlagen: " . $mysqli->connect_error);
}

//get paramter auslesen...
$edit_id = $_GET['id'];
$edit = false;

$query = "UPDATE `assignments` SET";

if(isset($_GET['gesture'])) {
	$gesture = $_GET['gesture'];
	$query .= " `gesture`='$gesture'";
	$edit = true;
}

if(isset($_GET['interface'])) {
	$interface = $_GET['interface'];
	$query .= " `interface`='$interface'";
	$edit = true;
}

if(isset($_GET['param'])) {
	$param = $_GET['param'];
	$query .= " `param`='$param'";
	$edit = true;
}

if(isset($_GET['action'])) {
	$action = $_GET['action'];
	$query .= " `action`='$action'";
	$edit = true;
}

$query .= " WHERE `id`=$edit_id";

//mysql query...
if($edit) {
$mysqli->query($query);
}

$output_data = array("success"=>"true");
echo json_encode($output_data);
?>