package de.kinemic.example.gesturereceiver;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import de.kinemic.toolbox.AdvancedGestureActivity;
import de.kinemic.toolbox.event.PublisherEvent;

public class MainActivity extends AdvancedGestureActivity {

    private TextView mSensorInfo;
    private TextView mStreamInfo;
    private TextView mLastInfo;
    private TextView mActiveInfo;
    private TextView mLastEventInfo;
    public Button buttonright;
    public Button buttonleft;
    public Button buttonup;
    public Button buttondown;
    public ListView listView;
    private List<smcDevice> device_list = new ArrayList<>();
    private int device_index = 0;

    // public String api_endpoint = "10.200.20.242";
    public String api_endpoint = "http://10.200.20.242/smc/gesture.php";
    public String deviceID = "1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSensorInfo = (TextView) findViewById(R.id.meta_sensor_info);
        mStreamInfo = (TextView) findViewById(R.id.meta_stream_info);
        mLastInfo = (TextView) findViewById(R.id.meta_last_info);
        mActiveInfo = (TextView) findViewById(R.id.meta_active_info);
        mLastEventInfo = (TextView) findViewById(R.id.event_info);
        buttondown = (Button) findViewById(R.id.buttondown);
        buttonleft = (Button) findViewById(R.id.buttonleft);
        buttonright = (Button) findViewById(R.id.buttonright);
        buttonup = (Button) findViewById(R.id.buttonup);
        listView = (ListView) findViewById(R.id.listview);


        buttondown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendGesture("Swipe Down");
            }
        });
        buttonup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendGesture("Swipe Up");
            }
        });
        buttonright.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendGesture("Swipe R");
            }
        });
        buttonleft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendGesture("Swipe L");
            }
        });

        getDevices();
        //anzeigen
    }

    @Override
    protected String getPublisherIP() {
        return "127.0.0.1";
    }

    @Override
    protected void handleEvent(final PublisherEvent event) throws JSONException {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Date now = new Date();
                try {
                    switch (event.type) {

                        case Gesture:
                            PublisherEvent.Gesture gesture = event.asGesture();

                            Log.d("GESTURE", "Gesture: " + gesture.name);
                            sendGesture(gesture.name);
                            mLastEventInfo.setText(gesture.name);
                            onGesture(gesture);
                            break;
                        case Writing:
                            break;
                        case MouseEvent:
                            break;
                        case Activation:
                            PublisherEvent.Activation activation = event.asActivation();
                            Log.d("Events", "Activation: " + activation.active);
                            mLastEventInfo.setText(now.toString() + "Activation: Stream " + (activation.active ? "resumed" : "paused"));
                            break;
                        case WritingSegment:
                            break;
                        case Heartbeat:
                            PublisherEvent.Heartbeat h = event.asHeartbeat();
                            Log.d("Events", "Heartbeat [active: " + h.active + ", last: " + h.last + ", stream: " + h.stream + ", sensor: " + h.sensor + "]");
                            mSensorInfo.setText(h.sensor);
                            mStreamInfo.setText(h.stream);
                            mLastInfo.setText("" + h.last);
                            mActiveInfo.setText("" + h.active);
                            break;
                    }
                } catch (JSONException e) {
                }
            }
        });
    }

    public void sendGesture(String gestureName) {
        int gestureID = 5; // Undefined

        // 1 SwipeUp
        // 2 SwipeDown

        if (gestureName.equals("Swipe Up")) {
            gestureID = 1;
        }
        if (gestureName.equals("Swipe Down")) {
            gestureID = 2;
        }
        if (gestureName.equals("Swipe R")) {
            gestureID = 3;
        }
        if (gestureName.equals("Swipe L")) {
            gestureID = 4;
        }
        if (gestureID == 1 || gestureID == 2) {
            String toSend = api_endpoint + "?gesture=" + String.valueOf(gestureID) + "&" + "device=" + String.valueOf(deviceID);

            RequestQueue queue = Volley.newRequestQueue(this);
            String url = toSend;

// Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Display the first 500 characters of the response string.
                            Log.d("ERFOLG", "Anfrage erfolgreich versendet!");
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", "Es ist ein Fehler aufgetreten!");
                }
            });
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        } else

        {

        }
    }



    public void getDevices() {
        device_list.clear();
            RequestQueue queue = Volley.newRequestQueue(this);
            String url = "http://10.200.20.242/smc/getDevices.php";

// Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            // Display the first 500 characters of the response string.
                            try {
                                JSONObject obj = new JSONObject(response);
                                JSONArray device_arr = obj.getJSONArray("devices");
                                for(int i=0; i<device_arr.length(); i++) {
                                    JSONObject device_obj = (JSONObject)device_arr.get(i);
                                    smcDevice smcd = new smcDevice(device_obj.getInt("id"),
                                            device_obj.getString("name"),
                                            device_obj.getString("description"),
                                            device_obj.getString("img_url"));
                                    device_list.add(smcd);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d("ERFOLG", "Anfrage erfolgreich versendet!");
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("ERROR", "Es ist ein Fehler aufgetreten!");
                }
            });
// Add the request to the RequestQueue.
            queue.add(stringRequest);
    }


    protected void onGesture(PublisherEvent.Gesture gesture) {
    }
    private class smcDevice {
        public smcDevice() {

        }
        public smcDevice(int id, String name, String description) {
            this.id = id;
            this.name = name;
            this.description = description;
        }
        public smcDevice(int id, String name, String description, String img_url) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.img_url = img_url;

        }
        public int id = -1;
        public String name = "";
        public String description = "";
        public String img_url = "";
    }
}
