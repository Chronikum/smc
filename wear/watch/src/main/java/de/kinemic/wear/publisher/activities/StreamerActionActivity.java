package de.kinemic.wear.publisher.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.wearable.view.ConfirmationOverlay;
import android.util.Log;

import java.io.File;

import de.kinemic.wear.publisher.service.Streamer;


public class StreamerActionActivity extends StreamerBaseActivity implements ConfirmationOverlay.FinishedAnimationListener {

    public static final String EXTRA_ACTION = "action";
    public static final int ACTION_START = 1;
    public static final int ACTION_STOP = 2;
    public static final int ACTION_PAUSE = 3;
    public static final int ACTION_RESUME = 4;

    private static final long SERVICE_TIMEOUT_MS = 1000L;
    private static final long ACTION_TIMEOUT_MS = 10000L;

    private int mAction;
    private Handler mHandler;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(android.support.wearable.R.style.ConfirmationActivity);

        mAction = getIntent().getIntExtra(EXTRA_ACTION, 0);
        Log.d("RecordActionActivity", "got action: " + mAction);
        if (mAction <= 0) throw new IllegalArgumentException("EXTRA_ACTION must be set to a proper value");

        mHandler = new Handler();
        mHandler.postDelayed(mServiceTimeout, SERVICE_TIMEOUT_MS);
    }

    private Runnable mServiceTimeout = new Runnable() {
        @Override
        public void run() {
            setResult(RESULT_CANCELED);
            showOverlay(ConfirmationOverlay.FAILURE_ANIMATION, "No Service");
        }
    };

    private Runnable mActionTimeout = new Runnable() {
        @Override
        public void run() {
            setResult(RESULT_CANCELED);
            showOverlay(ConfirmationOverlay.FAILURE_ANIMATION, "Failed (timeout)");
        }
    };

    private void showOverlay(int type, String message) {
        (new ConfirmationOverlay()).setType(type).setMessage(message).setFinishedAnimationListener(this).showOn(this);
    }

    public void onAnimationFinished() {
        this.finish();
    }

    protected void onConnected() {
        super.onConnected();
        Log.d("RecordActionActivity", "onConnect action: " + mAction);

        mHandler.removeCallbacks(mServiceTimeout);

        switch (mAction) {
            case ACTION_START:
                mStreamer.start("tcp://*:6789");
                break;
            case ACTION_STOP:
                mStreamer.stop();
                stopStreamerService();
                break;
            case ACTION_PAUSE:
                mStreamer.pause();
                break;
            case ACTION_RESUME:
                mStreamer.resume();
                break;
        }

        mHandler.postDelayed(mActionTimeout, ACTION_TIMEOUT_MS);
    }

    public void onStreamerStateChange(int state) {
        super.onStreamerStateChange(state);

        switch (mAction) {
            case ACTION_START:
                if (state == Streamer.STATE_STREAMING) {
                    setResult(RESULT_OK);
                    showOverlay(ConfirmationOverlay.SUCCESS_ANIMATION, "Started");
                }
                break;
            case ACTION_STOP:
                if (state == Streamer.STATE_STOPPED) {
                    setResult(RESULT_OK);
                    showOverlay(ConfirmationOverlay.SUCCESS_ANIMATION, "Stopped");
                }
                break;
            case ACTION_PAUSE:
                if (state == Streamer.STATE_PAUSED) {
                    setResult(RESULT_OK);
                    showOverlay(ConfirmationOverlay.SUCCESS_ANIMATION, "Paused");
                }
                break;
            case ACTION_RESUME:
                if (state == Streamer.STATE_STREAMING) {
                    setResult(RESULT_OK);
                    showOverlay(ConfirmationOverlay.SUCCESS_ANIMATION, "Resumed");
                }
                break;
        }
    }

}
