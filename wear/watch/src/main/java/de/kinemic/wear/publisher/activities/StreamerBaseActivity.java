package de.kinemic.wear.publisher.activities;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;

import java.io.File;

import de.kinemic.wear.publisher.service.Streamer;
import de.kinemic.wear.publisher.service.StreamerService;

public class StreamerBaseActivity extends Activity implements Streamer.OnStateChangeListener {

    protected Streamer mStreamer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService(new Intent(this, StreamerService.class), mServiceConnection, 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mStreamer != null) mStreamer.unregisterOnStateChangeListener(this);
        unbindService(mServiceConnection);
        mStreamer = null;
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mStreamer = ((StreamerService.LocalBinder) service).getStreamer();
            mStreamer.registerOnStateChangeListener(StreamerBaseActivity.this);
            onConnected();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mStreamer = null;
            onDisconnected();
        }
    };

    protected void onConnected() {

    }

    protected void onDisconnected() {

    }

    @Override
    public void onStreamerStateChange(@Streamer.StreamerState int state) {

    }

    protected void startStreamerService() {
        startService(new Intent(this, StreamerService.class));
    }

    protected void stopStreamerService() {
        stopService(new Intent(this, StreamerService.class));
    }
}
