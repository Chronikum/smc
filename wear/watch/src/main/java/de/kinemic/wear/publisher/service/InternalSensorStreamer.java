package de.kinemic.wear.publisher.service;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.util.Log;
import android.util.SparseIntArray;

import org.zeromq.ZMQ;
import org.zeromq.ZSocket;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentLinkedDeque;

import de.kinemic.internal.GesturePublisher;
import de.kinemic.io.AndroidResourceManager;

import static android.content.Context.POWER_SERVICE;


/**
 * Created by Fabian on 04.12.16.
 */

public class InternalSensorStreamer implements Streamer {
    static {
        System.loadLibrary("kinemic-publisher");
    }

    private static final String TAG = "InternalSensorStreamer";
    private static final String TAG_REPORT = "Streamer_Report";

    private final DateFormat mFilenameDateFormat = new SimpleDateFormat("yyyy-MM-dd--HH-mm", Locale.getDefault());
    private final DateFormat mDurationFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

    private static final int SENSOR_DELAY_US = SensorManager.SENSOR_DELAY_GAME;
    private static final int SENSOR_MAX_BATCH_DELAY_US = 1000000; // 1s;
    private static final long REPORT_DELAY_MS = 30 * 1000; // 5min

    // accessed from the main thread
    private @StreamerState int mState = STATE_STOPPED;
    private SparseIntArray mCounts;
    private ArrayList<OnStateChangeListener> mStateListeners;
    private SensorManager mSensorManager;
    private PowerManager.WakeLock mWakeLock;

    // accessed from the io thread
    private ZSocket mStreamer;
    private Date mStartDate, mEndDate;

    private Context mContext;
    private HandlerThread mIOThread;
    private Handler mIOHandler;
    private Handler mMainHandler;

    private Alarm mAlarm = new Alarm();

    private GesturePublisher mGesturePublisher;
    private final Object mPublisherLock = new Object();
    private AndroidResourceManager mResourceManager;

    private ConcurrentLinkedDeque<WearSample> mAccList;
    private ConcurrentLinkedDeque<WearSample> mGyroList;

    public InternalSensorStreamer(Context context) {
        mContext = context;
        mStateListeners = new ArrayList<>();
        mCounts = new SparseIntArray();

        mIOThread = new HandlerThread("StreamerIO");
        mIOThread.start();
        mIOHandler = new Handler(mIOThread.getLooper());
        mMainHandler = new Handler(Looper.getMainLooper());

        mAccList = new ConcurrentLinkedDeque<>();
        mGyroList = new ConcurrentLinkedDeque<>();

        /* mIOHandler.post(new Runnable() {
            @Override
            public void run() {
                migrateOldFolder();
            }
        }); */

        mIOHandler.postDelayed(mReportSamplesRunner, REPORT_DELAY_MS);

        PowerManager powerManager = (PowerManager) context.getSystemService(POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
    }

    public void quit() {
        stop();
        mIOHandler.removeCallbacks(mReportSamplesRunner);
        mIOHandler.removeCallbacks(mStreamRunner);
        mIOThread.quitSafely();
    }

    private final Runnable mReportSamplesRunner = new Runnable() {
        @Override
        public void run() {
            if (mState == STATE_STREAMING) {
                long duration = (new Date()).getTime() - mStartDate.getTime();
                Log.d(TAG_REPORT, "Duration: " + mDurationFormat.format(new Date(duration - TimeZone.getDefault().getRawOffset())));
                for (int i = 0; i < mCounts.size(); ++i) {
                    Log.d(TAG_REPORT, "Sensor " + mCounts.keyAt(i) + ": " + mCounts.valueAt(i) + " samples (" + (1000.f * mCounts.valueAt(i) / duration) + "Hz)");
                }
                //mGesturePublisher.setSensorStream("tcp://192.168.178.31:6789");
            }
            mIOHandler.postDelayed(this, REPORT_DELAY_MS);
        }
    };

    private final Runnable mStreamRunner = new Runnable() {
        @Override
        public void run() {
            if (mState == STATE_STREAMING) {
                while (!mAccList.isEmpty() && mGyroList.size() >= 2) {
                    WearSample acc = mAccList.removeFirst();
                    WearSample gyroLeft, gyroRight;

                    gyroLeft = mGyroList.removeFirst();
                    gyroRight = mGyroList.peekFirst();

                    while (gyroRight.timestamp < acc.timestamp) {
                        gyroLeft = mGyroList.removeFirst();
                        if (mGyroList.isEmpty()) {
                            mGyroList.addFirst(gyroLeft);
                            mAccList.addFirst(acc);
                            break;
                        }
                        gyroRight = mGyroList.peekFirst();
                    }

                    //todo: interpolate between left and right
                    mStreamer.send(WearSample.getProtoBytes(acc, gyroRight));
                    mGyroList.addFirst(gyroLeft);

                    int count = mCounts.get(42, 0);
                    mCounts.put(42, count + 1);
                }
            }
        }
    };

    private void startPublisher() {
        Thread starter = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (mPublisherLock) {
                    if (mGesturePublisher != null) {
                        mGesturePublisher.close();
                        mGesturePublisher = null;
                    }

                    mResourceManager = new AndroidResourceManager(mContext);
                    mResourceManager.getNativePath("models"); // FIXME: this is needed to extract the folder correctly
                    mResourceManager.setPrefix("models/");

                    mGesturePublisher = GesturePublisher.create(mResourceManager, "tcp://*:9999", "tcp://*:9998",
                            "std_19", "checklist", GesturePublisher.STD12_GESTURES | GesturePublisher.DEV_VERSION | GesturePublisher.VERBOSE, /* topics */ false);
                    mGesturePublisher.setSensorStream("tcp://127.0.0.1:6789");
                    mGesturePublisher.setGestureThreshold((float) GesturePublisher.THRESHOLD_SENSITIVE);
                }
            }
        });
        starter.start();
    }

    private void stopPublisher() {
        Thread stopper = new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (mPublisherLock) {
                    if (mGesturePublisher != null) {
                        mGesturePublisher.close();
                        mGesturePublisher = null;
                    }
                }
            }
        });
        stopper.start();
    }


    @Override
    public void start(final String endpoint) {
        if (mState != STATE_STOPPED) {
            stop();
        }

        if (mState == STATE_STOPPED) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "Start to stream");
            }
            mIOHandler.post(new Runnable() {
                @Override
                public void run() {
                    mStreamer = new ZSocket(ZMQ.PUB);
                    boolean success = mStreamer.bind(endpoint);

                    if (success) {
                        mStartDate = new Date();
                        mMainHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                startPublisher();
                                setState(STATE_PAUSED);
                                resume();
                            }
                        });
                    }
                }
            });
        }
    }

    @Override
    public void stop() {
        if (mState == STATE_STREAMING) {
            pause();
        }

        if (mState != STATE_STOPPED) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "Stop to stream");
            }

            mIOHandler.post(new Runnable() {
                @Override
                public void run() {
                    mStreamer.close();
                    mStreamer = null;
                    mEndDate = new Date();
                }
            });
            stopPublisher();
            setState(STATE_STOPPED);
        }
    }

    @Override
    public void pause() {
        if (mState == STATE_STREAMING) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "pause streaming.");
            }

            stopListening();
            mWakeLock.release();
            mAlarm.cancelAlarm(mContext);

            setState(STATE_PAUSED);
        }
    }

    @Override
    public void resume() {
        if (mState == STATE_PAUSED) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "resume streaming.");
            }

            setState(STATE_STREAMING);

            startListening();
            mWakeLock.acquire();
            mAlarm.setAlarm(mContext);
        }
    }


    @Override
    public void registerOnStateChangeListener(@NonNull OnStateChangeListener listener) {
        if (!mStateListeners.contains(listener)) {
            mStateListeners.add(listener);
            //listener.onStreamerStateChange(mState);
        }
    }

    @Override
    public void unregisterOnStateChangeListener(@NonNull OnStateChangeListener listener) {
        if (mStateListeners.contains(listener)) {
            mStateListeners.remove(listener);
        }
    }

    @Override
    public int getState() {
        return mState;
    }

    @Override
    public Date getStreamStartTime() {
        if (mStartDate == null) return null;
        return new Date(mStartDate.getTime());
    }

    private void setState(@StreamerState int state) {
        if (mState != state) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "changed state from " + mState + " to " + state);
            }

            mState = state;
            for (OnStateChangeListener listener : mStateListeners) {
                listener.onStreamerStateChange(state);
            }
        }
    }


    private void startListening() {
        mSensorManager = ((SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE));

        /*if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "Scanning Sensors...");
            List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
            Log.d(TAG, "Found " + sensors.size() + " sensors:");
            for (Sensor s : sensors) {
                Log.d(TAG, s.toString());
            }
        }*/

        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "Start to listen");
        }

        Sensor accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (Log.isLoggable(TAG, Log.INFO)) {
            Log.i(TAG, "Accelerometer fifo size: " + accelerometer.getFifoMaxEventCount());
        }
        Sensor gyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE_UNCALIBRATED);
        if (gyroscope == null) {
            if (Log.isLoggable(TAG, Log.INFO)) {
                Log.i(TAG, "No sensor of type: 'gyroscope_uncalibrated'. Fall back to 'gyroscope'");
            }
            gyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            if (Log.isLoggable(TAG, Log.INFO)) {
                Log.i(TAG, "Gyroscope fifo size: " + gyroscope.getFifoMaxEventCount());
            }
        }

        mSensorManager.registerListener(mSensorListener, accelerometer, SENSOR_DELAY_US, SENSOR_MAX_BATCH_DELAY_US);
        mSensorManager.registerListener(mSensorListener, gyroscope, SENSOR_DELAY_US, SENSOR_MAX_BATCH_DELAY_US);
    }

    private void stopListening() {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "Stop to listen");
        }
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(mSensorListener);
        }
    }

    private final SensorEventListener mSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (mState == STATE_STREAMING) {
                final WearSample sample = new WearSample(event);

                switch (sample.type) {
                    case Sensor.TYPE_ACCELEROMETER:
                        mAccList.addLast(sample);
                        mIOHandler.post(mStreamRunner);
                        break;
                    case Sensor.TYPE_GYROSCOPE:
                    case Sensor.TYPE_GYROSCOPE_UNCALIBRATED:
                        mGyroList.addLast(sample);
                        //mIOHandler.post(mStreamRunner);
                        break;
                }

                int count = mCounts.get(sample.type, 0);
                mCounts.put(sample.type, count + 1);
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, sensor.getStringType() + " changed accuracy to: " + accuracy);
            }
        }
    };
}
