package de.kinemic.wear.publisher.service;

import android.hardware.SensorEvent;
import android.os.Build;

import com.kinemic.kinemic.Sensor;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by Fabian on 30.11.16.
 */

public class WearSample {

    private static final int length = 24; // 4+8+4+4+4

    public int type;
    public float[] values;
    public long timestamp;

    public WearSample(SensorEvent event) {
        type = event.sensor.getType();
        values = event.values.clone();
        timestamp = event.timestamp;
    }

    private WearSample()  {
        type = 0;
        values = new float[3];
        timestamp = 0L;
    }

    public void writeTo(ByteBuffer byteBuffer) {
        byteBuffer.putInt(type);
        byteBuffer.putLong(timestamp);
        byteBuffer.putFloat(values[0]);
        byteBuffer.putFloat(values[1]);
        byteBuffer.putFloat(values[2]);
    }

    public static WearSample readFrom(ByteBuffer byteBuffer) {
        WearSample sample = new WearSample();
        sample.type = byteBuffer.getInt();
        sample.timestamp = byteBuffer.getLong();
        sample.values[0] = byteBuffer.getFloat();
        sample.values[1] = byteBuffer.getFloat();
        sample.values[2] = byteBuffer.getFloat();
        return sample;
    }

    public byte[] getBytes() {
        ByteBuffer buffer = ByteBuffer.allocate(length);
        writeTo(buffer);
        return buffer.array();
    }

    public static WearSample readFrom(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        return readFrom(buffer);
    }

    private static final float wr_acc_to_proto = -1000.f / 9.81f;
    private static final float wr_gyro_to_proto = 57.29577951308f;

    public static byte[] getProtoBytes(WearSample _acc, WearSample _gyro) {
        Sensor.SensorPacket.Builder packetbuilder = Sensor.SensorPacket.newBuilder();
        ArrayList<Sensor.SensorValue> sensorValues = new ArrayList<>();
        Sensor.SensorValue.Builder valbuilder = Sensor.SensorValue.newBuilder();
        Sensor.ValueTriple acc = Sensor.ValueTriple.newBuilder()
                .setX(_acc.values[0] * wr_acc_to_proto)
                .setY(_acc.values[1] * wr_acc_to_proto)
                .setZ(_acc.values[2] * wr_acc_to_proto)
                .build();
        Sensor.ValueTriple gyro = Sensor.ValueTriple.newBuilder()
                .setX(_gyro.values[0] * wr_gyro_to_proto)
                .setY(_gyro.values[1] * wr_gyro_to_proto)
                .setZ(_gyro.values[2] * wr_gyro_to_proto)
                .build();
        valbuilder.setAcc(acc);
        valbuilder.setGyro(gyro);
        sensorValues.add(valbuilder.build());
        packetbuilder.addAllValues(sensorValues);

        Sensor.MetaData.Builder mbuilder = Sensor.MetaData.newBuilder();
        mbuilder.setName(Build.MODEL);
        mbuilder.setSamplingRate(50);
        packetbuilder.setMetadata(mbuilder);

        return packetbuilder.build().toByteArray();
    }
}
