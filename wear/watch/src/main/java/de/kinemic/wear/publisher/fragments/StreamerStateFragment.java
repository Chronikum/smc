package de.kinemic.wear.publisher.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.wearable.activity.ConfirmationActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.ImageButton;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import de.kinemic.wear.publisher.activities.StreamerActionActivity;
import de.kinemic.wear.publisher.service.Streamer;
import de.kinemic.wear.publisher.service.StreamerService;

import de.kinemic.wear.publisher.R;
/**
 * Use the {@link StreamerStateFragment#create} factory method to
 * create an instance of this fragment.
 */

public class StreamerStateFragment extends StreamerBaseFragment {

    private static final int STATE_NOT_CONNECTED = -1;
    DateFormat mDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER

    // TODO: Rename and change types of parameters

    private TextView mTitleView;
    private TextView mContentView;
    private TextView mContent2View;
    private Chronometer mChronometer;
    private ImageButton mActionButton;

    public StreamerStateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment StreamerBaseFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StreamerStateFragment create() {
        StreamerStateFragment fragment = new StreamerStateFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        updateForState(STATE_NOT_CONNECTED);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_recorder_state, container, false);

        mTitleView = (TextView) view.findViewById(R.id.title);
        mContentView = (TextView) view.findViewById(R.id.content);
        mContent2View = (TextView) view.findViewById(R.id.content2);
        mChronometer = (Chronometer) view.findViewById(R.id.chronometer);
        mActionButton = (ImageButton) view.findViewById(R.id.contentButton);
        mActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getContext().startService(new Intent(getContext(), StreamerService.class));

                Intent startIntent = new Intent(getContext(), StreamerActionActivity.class);
                startIntent.putExtra(StreamerActionActivity.EXTRA_ACTION, StreamerActionActivity.ACTION_START);
                startIntent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.SUCCESS_ANIMATION);
                ((Activity) getContext()).startActivityForResult(startIntent, StreamerActionActivity.ACTION_START);
            }
        });

        updateForState(STATE_NOT_CONNECTED);

        return view;
    }

    private void updateForState(int state) {
        switch (state) {

            case STATE_NOT_CONNECTED:
                mTitleView.setText("Publisher");
                mContentView.setText("Start");
                break;
            case Streamer.STATE_STOPPED:
                mTitleView.setText("Publisher");
                mContentView.setText("Start");
                break;
            case Streamer.STATE_STREAMING:
                mTitleView.setText("Detecting");
                mContentView.setText("Started");
                mContent2View.setText(mDateFormat.format(mStreamer.getStreamStartTime()));
                long elapsedRealtimeOffset = System.currentTimeMillis() - SystemClock.elapsedRealtime();
                mChronometer.setBase(mStreamer.getStreamStartTime().getTime() - elapsedRealtimeOffset);
                mChronometer.start();
                break;
            case Streamer.STATE_PAUSED:
                mTitleView.setText("Publisher");
                mContentView.setText("Paused");
                break;

        }
        mActionButton.setVisibility(state == STATE_NOT_CONNECTED || state == Streamer.STATE_STOPPED ? View.VISIBLE : View.INVISIBLE);
        mChronometer.setVisibility(state == Streamer.STATE_STREAMING ? View.VISIBLE : View.INVISIBLE);
        mContent2View.setVisibility(state == Streamer.STATE_STREAMING ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void onStreamerStateChange(@Streamer.StreamerState int state) {
        super.onStreamerStateChange(state);
        updateForState(state);
    }

    @Override
    protected void onConnected() {
        super.onConnected();
        updateForState(mStreamer.getState());
    }
}
