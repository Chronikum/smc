/*
 * Copyright (C) 2014 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.kinemic.wear.publisher.fragments;

import android.animation.Animator;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.widget.TextView;

import java.io.IOException;

import de.kinemic.wear.publisher.R;
import pl.droidsonroids.gif.GifDrawable;
import pl.droidsonroids.gif.GifImageView;


/**
 * A simple fragment that shows a button to reset the counter
 */
public class GuideFragment extends StreamerBaseFragment {

    public static GuideFragment create() {
        GuideFragment fragment = new GuideFragment();
        return fragment;
    }

    boolean mActive = false;
    int mTrainGesture = 0;
    GifImageView mGifView;
    int[] mGifResources;
    int[] mGifDurations;
    View mConfirmationView;
    TextView mDescriptionView;
    TextView mTitleView;
    TextView mTryView;
    Handler mHandler;
    Vibrator mVibrator;

    private final String[] mGuidedGestures = new String[] {
            "Rotate RL", "Rotate LR", "Swipe R", "Swipe L", "Circle R", "Circle L",  "Check Mark", "X Mark"
    };

    private final int mLenght = mGuidedGestures.length;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.guide_layout, container, false);

        mVibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        mHandler = new Handler();
        mGifView = (GifImageView) view.findViewById(R.id.gifView);
        mDescriptionView = (TextView) view.findViewById(R.id.gesture_desc);
        mTitleView = (TextView) view.findViewById(R.id.title);
        mTryView = (TextView) view.findViewById(R.id.try_it);
        mConfirmationView = view.findViewById(R.id.confirmation);

        mGifResources = new int[mLenght];
        mGifDurations = new int[mLenght];
        for (int i = 0; i < mLenght; ++i) {
            mGifResources[i] = getResources().getIdentifier(mGuidedGestures[i].toLowerCase().replace(" ", "_"), "drawable", getActivity().getPackageName());
            try {
                GifDrawable gif = new GifDrawable(getResources(), mGifResources[i]);
                mGifDurations[i] = gif.getDuration();
            } catch (IOException e) {
                mGifDurations[i] = 1000;
            }
        }

        mConfirmationView.setVisibility(View.INVISIBLE);
        mTitleView.setVisibility(View.INVISIBLE);

        mDescriptionView.setVisibility(View.VISIBLE);
        mDescriptionView.setText(mGuidedGestures[mTrainGesture]);
        mTryView.setVisibility(View.VISIBLE);

        mGifView.setImageResource(mGifResources[mTrainGesture]);
        mGifView.setVisibility(View.VISIBLE);

        return view;
    }

    @Override
    protected void handleGesture(String gesture) {
        if (mGuidedGestures[mTrainGesture].equals(gesture)) {
            showConfirmation();
        } else {
            mVibrator.vibrate(new long[] {0, 35L}, -1);
        }
    }

    private void nextGesture() {
        mTrainGesture = (mTrainGesture + 1) % mLenght;

        mConfirmationView.setVisibility(View.INVISIBLE);
        mTitleView.setVisibility(View.INVISIBLE);

        mDescriptionView.setVisibility(View.VISIBLE);
        mDescriptionView.setText(mGuidedGestures[mTrainGesture]);
        mTryView.setVisibility(View.VISIBLE);

        mGifView.setImageResource(mGifResources[mTrainGesture]);
        mGifView.setVisibility(View.VISIBLE);
    }


    public void onAttach(Context context) {
        super.onAttach(context);
    }

    private void showConfirmation() {
        // get the center for the clipping circle
        int cx = mConfirmationView.getWidth() / 2;
        int cy = mConfirmationView.getHeight() / 2;

        // get the final radius for the clipping circle
        float finalRadius = (float) Math.hypot(cx, cy);

        // create the animator for this view (the start radius is zero)
        Animator anim = ViewAnimationUtils.createCircularReveal(mConfirmationView, cx, cy, 0, finalRadius);
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        nextGesture();
                    }
                }, 1000L);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        // make the view visible and start the animation
        mConfirmationView.setVisibility(View.VISIBLE);
        mDescriptionView.setVisibility(View.INVISIBLE);

        anim.start();

        mVibrator.vibrate(new long[] {0, 200L}, -1);
    }

}
