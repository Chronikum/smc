/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.kinemic.wear.publisher.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.support.wearable.activity.ConfirmationActivity;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.FragmentGridPagerAdapter;
import android.support.wearable.view.GridPagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.kinemic.wear.publisher.fragments.ActionFragment;
import de.kinemic.wear.publisher.fragments.GuideFragment;
import de.kinemic.wear.publisher.fragments.StreamerStateCardFragment;
import de.kinemic.wear.publisher.fragments.TransparentFinishFragment;
import de.kinemic.wear.publisher.service.Streamer;
import de.kinemic.wear.publisher.service.StreamerService;

import de.kinemic.wear.publisher.R;

/**
 * Constructs fragments as requested by the GridViewPager. For each row a different background is
 * provided.
 * <p>
 * Always avoid loading resources from the main thread. In this sample, the background images are
 * loaded from an background task and then updated using {@link #notifyRowBackgroundChanged(int)}
 * and {@link #notifyPageBackgroundChanged(int, int)}.
 */
public class StreamerGridPagerAdapter extends FragmentGridPagerAdapter {
    private static final int TRANSITION_DURATION_MILLIS = 100;

    public static final int STATE_NOT_CONNECTED = 3;

    private final Context mContext;
    private List<Row>[] mRows;
    private ColorDrawable mDefaultBg;
    private int mState = STATE_NOT_CONNECTED;

    private ColorDrawable mClearBg;

    public StreamerGridPagerAdapter(Context ctx, FragmentManager fm) {
        super(fm);
        mContext = ctx;

        mRows = new List[4];
        for (int i = 0; i < 4; ++i) {
            mRows[i] = createRowsForState(i);
        }

        mDefaultBg = new ColorDrawable(ctx.getResources().getColor(R.color.kinemic_gray));
        mClearBg = new ColorDrawable(ctx.getResources().getColor(android.R.color.transparent));
    }

    public void setState(int state) {
        mState = state;
        notifyDataSetChanged();
    }

    private List<Row> createRowsForState(int state) {
        List<Row> rows = new ArrayList<Row>();

        rows.add(new Row( TransparentFinishFragment.create() ));

        switch (state) {

            case STATE_NOT_CONNECTED:
                rows.add(
                    new Row(
                        StreamerStateCardFragment.create(),
                        ActionFragment.create(R.drawable.ic_play_arrow_white, R.string.start_record, mStartListener)
                ));
                break;
            case Streamer.STATE_STOPPED:
                rows.add(
                    new Row(
                        StreamerStateCardFragment.create(),
                        ActionFragment.create(R.drawable.ic_play_arrow_white, R.string.start_record, mStartListener),
                        ActionFragment.create(R.drawable.ic_full_clear_white, R.string.stop_recorder, mStopStreamerListener)
                ));
                break;
            case Streamer.STATE_STREAMING:
                rows.add(
                    new Row(
                        //StreamerStateCardFragment.create(),
                        GuideFragment.create(),
                        ActionFragment.create(R.drawable.ic_full_clear_white, R.string.stop_recorder, mStopStreamerListener)
                        //ActionFragment.create(R.drawable.ic_full_save_white, R.string.save_record, mSaveListener)
                        //ActionFragment.create(R.drawable.ic_full_cancel_white, R.string.cancel_record, mCancelListener)
                ));
                break;
            case Streamer.STATE_PAUSED:
                rows.add(
                    new Row(
                        StreamerStateCardFragment.create(),
                        ActionFragment.create(R.drawable.ic_full_clear_white, R.string.stop_recorder, mStopStreamerListener)
                        // ActionFragment.create(R.drawable.ic_full_cancel_white, R.string.discard_record, mDiscardListener),
                        // ActionFragment.create(R.drawable.ic_full_clear_white, R.string.stop_recorder, mStopStreamerListener)
                ));
                break;
        }

        return rows;
    }


    private ActionFragment.Listener mStartListener = new ActionFragment.Listener() {
        @Override
        public void onActionPerformed() {
            Log.d("GridPager", "start!");
            mContext.startService(new Intent(mContext, StreamerService.class));

            Intent startIntent = new Intent(mContext, StreamerActionActivity.class);
            startIntent.putExtra(StreamerActionActivity.EXTRA_ACTION, StreamerActionActivity.ACTION_START);
            startIntent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.SUCCESS_ANIMATION);
            ((Activity) mContext).startActivityForResult(startIntent, StreamerActionActivity.ACTION_START);
        }
    };

    private ActionFragment.Listener mStopListener = new ActionFragment.Listener() {
        @Override
        public void onActionPerformed() {
            Log.d("GridPager", "stop!");
            Intent stoptIntent = new Intent(mContext, StreamerActionActivity.class);
            stoptIntent.putExtra(StreamerActionActivity.EXTRA_ACTION, StreamerActionActivity.ACTION_STOP);
            ((Activity) mContext).startActivityForResult(stoptIntent, StreamerActionActivity.ACTION_STOP);
        }
    };

    private ActionFragment.Listener mPauseListener = new ActionFragment.Listener() {
        @Override
        public void onActionPerformed() {
            Log.d("GridPager", "pause!");
            Intent discardIntent = new Intent(mContext, StreamerActionActivity.class);
            discardIntent.putExtra(StreamerActionActivity.EXTRA_ACTION, StreamerActionActivity.ACTION_PAUSE);
            ((Activity) mContext).startActivityForResult(discardIntent, StreamerActionActivity.ACTION_PAUSE);
        }
    };

    private ActionFragment.Listener mResumeListener = new ActionFragment.Listener() {
        @Override
        public void onActionPerformed() {
            Log.d("GridPager", "resume!");
            Intent savetIntent = new Intent(mContext, StreamerActionActivity.class);
            savetIntent.putExtra(StreamerActionActivity.EXTRA_ACTION, StreamerActionActivity.ACTION_RESUME);
            ((Activity) mContext).startActivityForResult(savetIntent, StreamerActionActivity.ACTION_RESUME);
        }
    };

    private ActionFragment.Listener mStopStreamerListener = new ActionFragment.Listener() {
        @Override
        public void onActionPerformed() {
            Log.d("GridPager", "stop service!");
            mContext.stopService(new Intent(mContext, StreamerService.class));
            ((Activity) mContext).finish();
        }
    };

    LruCache<Integer, Drawable> mRowBackgrounds = new LruCache<Integer, Drawable>(3) {
        @Override
        protected Drawable create(final Integer row) {
            int resid = BG_IMAGES[row % BG_IMAGES.length];

            //if (row == 0) return mClearBg;

            new DrawableLoadingTask(mContext) {
                @Override
                protected void onPostExecute(Drawable result) {
                    TransitionDrawable background = new TransitionDrawable(new Drawable[] {
                            mDefaultBg,
                            result
                    });
                    mRowBackgrounds.put(row, background);
                    notifyRowBackgroundChanged(row);
                    background.startTransition(TRANSITION_DURATION_MILLIS);
                }
            }.execute(resid);
            return mDefaultBg;
        }
    };

    LruCache<Point, Drawable> mPageBackgrounds = new LruCache<Point, Drawable>(3) {
        @Override
        protected Drawable create(final Point page) {
            // place bugdroid as the background at row 2, column 1
            /*if (false) {
                int resid = any;
                new DrawableLoadingTask(mContext) {
                    @Override
                    protected void onPostExecute(Drawable result) {
                        TransitionDrawable background = new TransitionDrawable(new Drawable[] {
                                mClearBg,
                                result
                        });
                        mPageBackgrounds.put(page, background);
                        notifyPageBackgroundChanged(page.y, page.x);
                        background.startTransition(TRANSITION_DURATION_MILLIS);
                    }
                }.execute(resid);
            }*/
            return GridPagerAdapter.BACKGROUND_NONE;
        }
    };

    private Fragment cardFragment(int titleRes, int textRes) {
        Resources res = mContext.getResources();
        CardFragment fragment =
                CardFragment.create(res.getText(titleRes), res.getText(textRes));
        // Add some extra bottom margin to leave room for the page indicator
        fragment.setCardMarginBottom(
                res.getDimensionPixelSize(R.dimen.card_margin_bottom));
        return fragment;
    }

    static final int[] BG_IMAGES = new int[] {
            R.drawable.background1,
    };

    /** A convenient container for a row of fragments. */
    private class Row {
        final List<Fragment> columns = new ArrayList<Fragment>();

        public Row(Fragment... fragments) {
            for (Fragment f : fragments) {
                add(f);
            }
        }

        public void add(Fragment f) {
            columns.add(f);
        }

        Fragment getColumn(int i) {
            return columns.get(i);
        }

        public int getColumnCount() {
            return columns.size();
        }
    }

    @Override
    public Fragment getFragment(int row, int col) {
        Row adapterRow = mRows[mState].get(row);
        return adapterRow.getColumn(col);
    }

    @Override
    public Drawable getBackgroundForRow(final int row) {
        return mRowBackgrounds.get(row);
    }

    @Override
    public Drawable getBackgroundForPage(final int row, final int column) {
        return mPageBackgrounds.get(new Point(column, row));
    }

    @Override
    public int getRowCount() {
        return mRows[mState].size();
    }

    @Override
    public int getColumnCount(int rowNum) {
        return mRows[mState].get(rowNum).getColumnCount();
    }

    class DrawableLoadingTask extends AsyncTask<Integer, Void, Drawable> {
        private static final String TAG = "Loader";
        private Context context;

        DrawableLoadingTask(Context context) {
            this.context = context;
        }

        @Override
        protected Drawable doInBackground(Integer... params) {
            Log.d(TAG, "Loading asset 0x" + Integer.toHexString(params[0]));
            return context.getResources().getDrawable(params[0]);
        }
    }
}
