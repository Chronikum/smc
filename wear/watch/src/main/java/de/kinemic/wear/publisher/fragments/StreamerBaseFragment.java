package de.kinemic.wear.publisher.fragments;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import de.kinemic.publisher.app.ZMQEventReceiver;
import de.kinemic.wear.publisher.service.Streamer;
import de.kinemic.wear.publisher.service.StreamerService;

public class StreamerBaseFragment extends Fragment implements Streamer.OnStateChangeListener, Focusable {
    private static final String TAG = "StreamerBaseFragment";

    protected Streamer mStreamer;
    private Receiver mReceiver = new Receiver();
    private boolean mResumed = false;
    private boolean mFocused = false;
    private boolean mNeedsFocus = true;
    private Vibrator mVibrator;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mVibrator = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onAttach()");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onDetach()");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onResume()");
        }
        getActivity().bindService(new Intent(getActivity(), StreamerService.class), mStreamerServiceConnection, 0);

        mResumed = true;
        if (mFocused || !mNeedsFocus) mReceiver.start();
    }

    public void onGainFocus() {
        mFocused = true;
        if (mNeedsFocus && mResumed) mReceiver.start();
    }

    public void onLooseFocus() {
        mFocused = false;
        if (mNeedsFocus) mReceiver.stop();
    }

    public void setNeedsFocus(boolean needsFocus) {
        if (mNeedsFocus && !needsFocus && mResumed) mReceiver.start();
        if (!mNeedsFocus && needsFocus && mResumed) mReceiver.stop();

        mNeedsFocus = needsFocus;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onPause()");
        }
        if (mStreamer != null) mStreamer.unregisterOnStateChangeListener(this);
        getActivity().unbindService(mStreamerServiceConnection);
        mStreamer = null;

        mResumed = false;
        mReceiver.stop();
    }

    private ServiceConnection mStreamerServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "onServiceConnected()");
            }
            mStreamer = ((StreamerService.LocalBinder) service).getStreamer();
            mStreamer.registerOnStateChangeListener(StreamerBaseFragment.this);
            onConnected();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "onServiceDisconnected()");
            }
            mStreamer = null;
            onDisconnected();
        }
    };


    protected void onConnected() {

    }

    protected void onDisconnected() {

    }

    @Override
    public void onStreamerStateChange(@Streamer.StreamerState int state) {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onStreamerStateChange( " + state + " )");
        }
    }

    protected void handleGesture(String gesture) {
        Toast.makeText(getActivity(), gesture, Toast.LENGTH_SHORT).show();
        mVibrator.vibrate(new long[] {0, 35L}, -1);
    }

    protected void handleWriting(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_SHORT).show();
    }

    protected void handleLog(String level, String who, String message, long timestamp) {

    }

    private class Receiver extends ZMQEventReceiver {
        public Receiver() {
            super(false);
            //setLogLevels("info", "debug");
        }

        @Override
        protected void handleLog(String topic, String json) {
            if (getActivity() == null) return;
            try {
                final JSONObject log = new JSONObject(json).getJSONObject("parameters");
                final String level = log.getString("level");
                final String who = log.getString("who");
                final String msg = log.getString("message");
                final long timestamp = log.getLong("timestamp");
                Log.d("Logger", String.format("[%5s] %-12s  %s", level, who, msg));
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        StreamerBaseFragment.this.handleLog(level, who, msg, timestamp);
                    }
                });
            } catch (JSONException e) {
                Log.w("Logs", "Could not parse json: " + json, e);
            }
        }

        @Override
        protected void handleEvent(String type, String json) {
            if (getActivity() == null) return;

            try {
                JSONObject event = new JSONObject(json);
                final JSONObject params = event.getJSONObject("parameters");
                type = event.getString("type");

                if ("Heartbeat".equals(type)) {
                    boolean active = params.getBoolean("active");
                    int flags = params.getInt("flags");
                    long last = params.getLong("last");
                    String sensor = params.getString("sensor");
                    String stream = params.getString("stream");

                    Log.d("Events", "Heartbeat [active: " + active + ", last: " + last + ", stream: " + stream + ", sensor: " + sensor +  "]");
                    //updateMetaData(stream, sensor, flags, last, active);
                } else if ("Gesture".equals(type)) {
                    final String gesture = params.getString("name");
                    Log.d("Events", "Gesture: " + gesture);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            handleGesture(gesture);
                        }
                    });
                } else if ("Writing".equals(type)) {
                    final String hypo = params.getString("hypothesis");
                    Log.d("Events", "Writing: " + hypo);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            handleWriting(hypo);
                        }
                    });
                } else if ("Activation".equals(type)) {
                    final boolean active = params.getBoolean("active");
                    Log.d("Events", "Activation: " + active);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), active ? "Activated" : "Deactivated", Toast.LENGTH_SHORT).show();
                        }
                    });
                } else {
                    //Log.w("Events", "Unknown type: " + type);
                }
            } catch (JSONException e) {
                Log.w("Events", "Could not parse json: " + json, e);
            }
        }
    };
}
