package de.kinemic.wear.publisher.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.wearable.activity.ConfirmationActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import de.kinemic.wear.publisher.activities.StreamerActionActivity;
import de.kinemic.wear.publisher.activities.StreamerActivity;
import de.kinemic.wear.publisher.activities.StreamerNotificationActivity;

import de.kinemic.wear.publisher.R;

/**
 * Created by Fabian on 04.12.16.
 */

public class StreamerService extends Service implements Streamer.OnStateChangeListener {

    private final DateFormat mDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

    private static final int NOTIFICATION_ID = 153373;

    private final LocalBinder mBinder = new LocalBinder();
    private InternalSensorStreamer mStreamer;
    private NotificationManager mNotificationManager;
    private boolean mForeground = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    public void onCreate() {
        super.onCreate();

        mStreamer = new InternalSensorStreamer(getApplicationContext());
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        onStreamerStateChange(mStreamer.getState());
        mStreamer.registerOnStateChangeListener(this);
    }

    public void onDestroy() {
        super.onDestroy();

        mStreamer.quit();
        mStreamer.unregisterOnStateChangeListener(this);

        if (mNotificationManager != null) mNotificationManager.cancel(NOTIFICATION_ID);
        if (mForeground) stopForeground(true);
    }

    @Override
    public void onStreamerStateChange(@Streamer.StreamerState int state) {
        if (mForeground) stopForeground(true);
        if (mNotificationManager != null) mNotificationManager.cancel(NOTIFICATION_ID);

        Notification notification = createNotification(state);
        if (state == Streamer.STATE_STREAMING) {
            startForeground(NOTIFICATION_ID, notification);
            mForeground = true;
        } else if (state == Streamer.STATE_STOPPED) {
            mNotificationManager.notify(NOTIFICATION_ID, notification);
        } else if (state == Streamer.STATE_PAUSED) {
            //mNotificationManager.notify(NOTIFICATION_ID, notification);
        }
    }

    public class LocalBinder extends Binder {
        public Streamer getStreamer() {
            return mStreamer;
        }
    }

    private Notification createNotification(@Streamer.StreamerState int state) {
        Intent startIntent = new Intent(this, StreamerActionActivity.class);
        //Intent startIntent = new Intent(this, ConfirmationActivity.class);
        startIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION );
        startIntent.putExtra(StreamerActionActivity.EXTRA_ACTION, StreamerActionActivity.ACTION_START);
        startIntent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.SUCCESS_ANIMATION);
        PendingIntent startPendingIntent = PendingIntent.getActivity(this, 0, startIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action startAction = new Notification.Action.Builder(R.drawable.ic_play_arrow_white, "Start", startPendingIntent).build();

        Intent stoptIntent = new Intent(this, StreamerActionActivity.class);
        stoptIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        stoptIntent.putExtra(StreamerActionActivity.EXTRA_ACTION, StreamerActionActivity.ACTION_STOP);
        PendingIntent stopPendingIntent = PendingIntent.getActivity(this, 1, stoptIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action stopAction = new Notification.Action.Builder(R.drawable.ic_full_stop_white, "Stop", stopPendingIntent).build();

        Intent pauseIntent = new Intent(this, StreamerActionActivity.class);
        pauseIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        pauseIntent.putExtra(StreamerActionActivity.EXTRA_ACTION, StreamerActionActivity.ACTION_PAUSE);
        PendingIntent pausePendingIntent = PendingIntent.getActivity(this, 2, pauseIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action pauseAction = new Notification.Action.Builder(R.drawable.ic_full_cancel_white, "Pause", pausePendingIntent).build();

        Intent resumeIntent = new Intent(this, StreamerActionActivity.class);
        resumeIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        resumeIntent.putExtra(StreamerActionActivity.EXTRA_ACTION, StreamerActionActivity.ACTION_RESUME);
        PendingIntent resumePendingIntent = PendingIntent.getActivity(this, 3, resumeIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action resumeAction = new Notification.Action.Builder(R.drawable.ic_play_arrow_white, "Resume", resumePendingIntent).build();

        Intent displayIntent = new Intent(this, StreamerNotificationActivity.class);
        PendingIntent displayPendingIntent = PendingIntent.getActivity(this, 5, displayIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent guideIntent = new Intent(this, StreamerActivity.class);
        guideIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent guidePendingIntent = PendingIntent.getActivity(this, 3, guideIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action guideAction = new Notification.Action.Builder(R.drawable.ic_full_fitness_center_white_24dp, "Guide", guidePendingIntent).build();

        ArrayList<Notification.Action> actions = new ArrayList<>();

        String title = "";
        String text = "Swipe right for actions";
        boolean ongoing = false;
        switch (state) {
            case Streamer.STATE_STOPPED:
                actions.add(startAction);
                title = "Publisher";
                ongoing = false;
                break;
            case Streamer.STATE_STREAMING:
                actions.add(guideAction);
                actions.add(stopAction);
                //actions.add(stopAction);
                //actions.add(saveAction);
                //actions.add(cancelAction);
                title = "Detecting";
                text = "Started: " + mDateFormat.format(mStreamer.getStreamStartTime());
                ongoing = true;
                break;
            case Streamer.STATE_PAUSED:
                actions.add(stopAction);
                //actions.add(discardAction);
                title = "Publisher paused";
                ongoing = true;
                break;
        }


        Notification.WearableExtender extender = new Notification.WearableExtender()
                .addActions(actions)
                .setDisplayIntent(displayPendingIntent)
                .setCustomSizePreset(Notification.WearableExtender.SIZE_SMALL)
                .setBackground(BitmapFactory.decodeResource(getResources(), R.drawable.background1));
        //if (state == Streamer.STATE_STREAMING) extender.setContentAction(0);

        Notification.Builder builder = new Notification.Builder(getApplicationContext())
                .setContentTitle(title)
                .setSmallIcon(R.drawable.kinemic_icon_color)
                .extend(extender)
                .setOngoing(ongoing);
        return builder.build();
    }
}
