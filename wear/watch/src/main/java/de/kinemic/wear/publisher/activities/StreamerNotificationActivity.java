package de.kinemic.wear.publisher.activities;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import de.kinemic.wear.publisher.fragments.StreamerStateFragment;

import de.kinemic.wear.publisher.R;

/**
 * Created by Fabian on 04.12.16.
 */

public class StreamerNotificationActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_placeholder);

        StreamerStateFragment fragment = StreamerStateFragment.create();
        fragment.setNeedsFocus(false);

        getFragmentManager().beginTransaction()
                .replace(R.id.container, fragment).commit();
    }
}
