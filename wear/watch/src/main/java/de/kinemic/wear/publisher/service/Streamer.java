package de.kinemic.wear.publisher.service;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.annotation.Retention;
import java.util.Date;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by Fabian on 04.12.16.
 */

public interface Streamer {

    @Retention(SOURCE)
    @IntDef({STATE_STOPPED, STATE_PAUSED, STATE_STREAMING})
    @interface StreamerState{}

    /**
     * Nothing streaming, no resources allocated.
     */
    int STATE_STOPPED = 0;

    /**
     * Nothing streaming, resources still allocated.
     */
    int STATE_PAUSED = 1;

    /**
     * Streaming state.
     */
    int STATE_STREAMING = 2;

    /**
     * Listener to be notified about state changes of the streamer.
     */
    static interface OnStateChangeListener {
        /**
         * State of the streamer has changed.
         * State can be any of {@link Streamer#STATE_STOPPED}, {@link Streamer#STATE_PAUSED} or {@link Streamer#STATE_STREAMING}.
         * @param state new state
         */
        void onStreamerStateChange(@StreamerState int state);
    }

    /**
     * Start streamer at the given endpoint (zmq notation: tcp://..., inproc://...)
     */
    void start(String endpoint);

    /**
     * Resume streaming.
     */
    void resume();

    /**
     * pause streaming.
     */
    void pause();

    /**
     * Stop streaming and release resources.
     */
    void stop();

    /**
     * Register listener for changes in the recording state
     * @param listener the listener
     */
    void registerOnStateChangeListener(@NonNull OnStateChangeListener listener);

    /**
     * Unregister listener for changes in the recording state
     * @param listener the listener
     */
    void unregisterOnStateChangeListener(@NonNull OnStateChangeListener listener);

    /**
     * Return the current state of the streamer.
     * State can be any of {@link Streamer#STATE_STOPPED}, {@link Streamer#STATE_PAUSED} or {@link Streamer#STATE_STREAMING}.
     * @return current state
     */
    @StreamerState int getState();

    /**
     * Return the time when the current record was started.
     * @return the start time
     */
    @Nullable Date getStreamStartTime();
}
