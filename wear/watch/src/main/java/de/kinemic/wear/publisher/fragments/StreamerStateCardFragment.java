package de.kinemic.wear.publisher.fragments;

import android.os.Bundle;
import android.support.wearable.view.CardFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.kinemic.wear.publisher.R;

/**
 * Created by Fabian on 05.12.16.
 */

public class StreamerStateCardFragment extends CardFragment implements Focusable {

    private StreamerStateFragment mContent;

    public static StreamerStateCardFragment create() {
        StreamerStateCardFragment fragment = new StreamerStateCardFragment();
        return fragment;
    }

    @Override
    public View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_frame_placeholder, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mContent = StreamerStateFragment.create();
        getChildFragmentManager().beginTransaction().replace(R.id.container, mContent).commit();
    }

    public void onGainFocus() {
        mContent.onGainFocus();
    }

    public void onLooseFocus() {
        mContent.onLooseFocus();
    }
}
