package de.kinemic.wear.publisher.fragments;

import android.app.Fragment;

/**
 * Created by Fabian on 05.12.16.
 */

public class TransparentFinishFragment extends Fragment {

    public static TransparentFinishFragment create() {
        TransparentFinishFragment fragment = new TransparentFinishFragment();
        return fragment;
    }
}
