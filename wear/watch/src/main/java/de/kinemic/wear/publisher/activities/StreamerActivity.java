package de.kinemic.wear.publisher.activities;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.DataSetObserver;
import android.os.Handler;
import android.os.IBinder;
import android.support.wearable.view.GridPagerAdapter;
import android.support.wearable.view.GridViewPager;
import android.util.Log;

import de.kinemic.wear.publisher.fragments.Focusable;
import de.kinemic.wear.publisher.fragments.StreamerBaseFragment;
import de.kinemic.wear.publisher.service.Streamer;
import de.kinemic.wear.publisher.service.StreamerService;

/**
 * Created by Fabian on 05.12.16.
 */

public class StreamerActivity extends AbstractGridActivity implements Streamer.OnStateChangeListener {

    GridViewPager mPager;
    private Streamer mStreamer;
    private int mActiveCol = 0;
    private int mActiveRow = 0;

    @Override
    protected GridPagerAdapter createGridPageAdapter(Context context, FragmentManager manager) {
        return new StreamerGridPagerAdapter(context, manager);
    }

    @Override
    protected void onPostSetup(final GridViewPager pager) {
        super.onPostSetup(pager);
        mPager = pager;

        mPager.getAdapter().registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                Fragment next = ((StreamerGridPagerAdapter) mPager.getAdapter()).getFragment(mActiveRow, mActiveCol);
                if (next instanceof Focusable) {
                    ((Focusable) next).onGainFocus();
                }
            }
        });

        mPager.setOnPageChangeListener(new GridViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, int i1, float v, float v1, int i2, int i3) {

            }

            @Override
            public void onPageSelected(int row, int col) {
                Fragment prev = ((StreamerGridPagerAdapter) mPager.getAdapter()).getFragment(mActiveRow, mActiveCol);
                if (prev instanceof Focusable) {
                    ((Focusable) prev).onLooseFocus();
                }
                mActiveCol = col;
                mActiveRow = row;
                Fragment next = ((StreamerGridPagerAdapter) mPager.getAdapter()).getFragment(mActiveRow, mActiveCol);
                if (next instanceof Focusable) {
                    ((Focusable) next).onGainFocus();
                }

                // imitate the 'dismiss by pulling down' interaction, found in notifications
                if (row == 0 && col == 0) {
                    finish();
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        /* part of the 'dismiss by pulling down' workaround */
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("Pager", "set current item runnable");
                mPager.setCurrentItem(1, 0);
                mPager.getAdapter().notifyDataSetChanged();
            }
        }, 100L);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService(new Intent(this, StreamerService.class), mSericeConnection, 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mStreamer != null) mStreamer.unregisterOnStateChangeListener(this);
        unbindService(mSericeConnection);
        mStreamer = null;
        ((StreamerGridPagerAdapter) mPager.getAdapter()).setState(StreamerGridPagerAdapter.STATE_NOT_CONNECTED);
    }

    private ServiceConnection mSericeConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mStreamer = ((StreamerService.LocalBinder) service).getStreamer();
            mStreamer.registerOnStateChangeListener(de.kinemic.wear.publisher.activities.StreamerActivity.this);
            ((StreamerGridPagerAdapter) mPager.getAdapter()).setState(mStreamer.getState());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mStreamer = null;
            ((StreamerGridPagerAdapter) mPager.getAdapter()).setState(StreamerGridPagerAdapter.STATE_NOT_CONNECTED);
        }
    };

    @Override
    public void onStreamerStateChange(@Streamer.StreamerState int state) {
        ((StreamerGridPagerAdapter) mPager.getAdapter()).setState(state);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        //finish();
    }
}
