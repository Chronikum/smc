package de.kinemic.wear.publisher.fragments;

/**
 * Created by Fabian on 19.10.17.
 */

public interface Focusable {
    void onGainFocus();
    void onLooseFocus();
}
