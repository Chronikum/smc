package de.kinemic.wear.publisher.activities;

import android.app.Activity;
import android.os.Bundle;

import de.kinemic.wear.publisher.R;
import de.kinemic.wear.publisher.fragments.GuideFragment;
import de.kinemic.wear.publisher.fragments.StreamerStateFragment;

/**
 * Created by Fabian on 04.12.16.
 */

public class GuideActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_placeholder);
        getFragmentManager().beginTransaction()
                .replace(R.id.container, GuideFragment.create()).commit();
    }
}
