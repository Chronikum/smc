package de.kinemic.publisher.app;

import org.zeromq.ZContext;
import org.zeromq.ZLoop;
import org.zeromq.ZMQ;
import org.zeromq.ZMQException;
import org.zeromq.ZSocket;
import org.zeromq.ZThread;

import java.util.ArrayList;
import java.util.Collections;

import static org.zeromq.ZSocket.UTF8;

/**
 * Created by Fabian on 05.04.17.
 */

public abstract class ZMQEventReceiver {

    private final boolean mUseTopics;
    private ArrayList<String> mTopics;
    private ArrayList<String> mLogLevels;
    private ZMQ.Socket mPipe;
    private final Object mSignal = new Object();

    public ZMQEventReceiver(boolean useTopics) {
        mUseTopics = useTopics;
        mTopics = new ArrayList<>();
        mTopics.add("");
        mLogLevels = new ArrayList<>();
    }

    public void setLogLevels(String... levels) {
        mLogLevels.clear();
        Collections.addAll(mLogLevels, levels);
    }

    public void setEventTypes(String... types) {
        mTopics.clear();
        Collections.addAll(mTopics, types);
    }

    private ZThread.IAttachedRunnable mLoopRunnable = new ZThread.IAttachedRunnable() {
        @Override
        public void run(Object[] args, ZContext ctx, ZMQ.Socket pipe) {
            ZLoop loop = new ZLoop(ctx);

            loop.addPoller(new ZMQ.PollItem(pipe, ZMQ.Poller.POLLIN), mTermHandler, null);

            ZMQ.PollItem logPoller = null;
            if (mLogLevels.size() > 0) {
                ZMQ.Socket logSub = ctx.createSocket(ZMQ.SUB);
                logSub.connect("tcp://localhost:9996");
                for (String level : mLogLevels) {
                    logSub.subscribe(level.getBytes(UTF8));
                }
                logPoller = new ZMQ.PollItem(logSub, ZMQ.Poller.POLLIN);
                loop.addPoller(logPoller, mLogHandler, null);
            }

            ZMQ.Socket eventSub = ctx.createSocket(ZMQ.SUB);
            eventSub.connect("tcp://localhost:9999");
            for (String level : mTopics) {
                eventSub.subscribe(level.getBytes(UTF8));
            }
            ZMQ.PollItem eventPoller = new ZMQ.PollItem(eventSub, ZMQ.Poller.POLLIN);
            loop.addPoller(eventPoller, mEventHandler, null);

            synchronized (mSignal) {
                mSignal.notify();
            }

            loop.start();

            loop.removePoller(eventPoller);
            if (logPoller != null) {
                loop.removePoller(logPoller);
            }

            synchronized (mSignal) {
                mSignal.notify();
            }
        }
    };

    public void stop() {
        Thread stopper = new Thread(new Runnable() {
            @Override
            public void run() {
                if (mPipe != null) {
                    mPipe.setSendTimeOut(0);
                    boolean success = mPipe.send("$TERM");
                    if (success) {
                        try {
                            synchronized (mSignal) {
                                mSignal.wait();
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    mPipe = null;
                }
            }
        });
        stopper.start();
        try {
            stopper.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void start() {
        Thread starter = new Thread(new Runnable() {
            @Override
            public void run() {
                if (mPipe != null) stop();

                mPipe = ZThread.fork(new ZContext(), mLoopRunnable);

                try {
                    synchronized (mSignal) {
                        mSignal.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        starter.start();
        try {
            starter.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private ZLoop.IZLoopHandler mTermHandler = new ZLoop.IZLoopHandler() {
        @Override
        public int handle(ZLoop loop, ZMQ.PollItem item, Object arg) {
            final String command = item.getSocket().recvStr();
            if (command.equals("$TERM")) return -1;
            return 0;
        }
    };

    private ZLoop.IZLoopHandler mLogHandler = new ZLoop.IZLoopHandler() {
        @Override
        public int handle(ZLoop loop, ZMQ.PollItem item, Object arg) {
            // message contains 2 parts
            final String level = item.getSocket().recvStr();
            if (item.getSocket().hasReceiveMore()) {
                final String jsonLog = item.getSocket().recvStr();
                handleLog(level, jsonLog);
            }
            return 0;
        }
    };

    private ZLoop.IZLoopHandler mEventHandler = new ZLoop.IZLoopHandler() {
        @Override
        public int handle(ZLoop loop, ZMQ.PollItem item, Object arg) {
            if (mUseTopics) {
                final String topic = item.getSocket().recvStr();
                if (item.getSocket().hasReceiveMore()) {
                    final String jsonEvent = item.getSocket().recvStr().replace("null", "{}");

                    handleEvent(topic, jsonEvent);
                }
            } else {
                final String jsonEvent = item.getSocket().recvStr().replace("null", "{}");;
                handleEvent("", jsonEvent);
            }

            return 0;
        }
    };

    protected abstract void handleLog(String level, String json);

    protected abstract void handleEvent(String topic, String event);
}
