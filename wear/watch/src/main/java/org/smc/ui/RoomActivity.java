package org.smc.ui;

import android.os.Bundle;
import android.support.wearable.activity.WearableActivity;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.smc.RoomView;
import org.smc.model.Room;

import de.kinemic.wear.publisher.R;

public class RoomActivity extends WearableActivity {

    private TextView mTextView;
    private RoomView mRoomView;
    private Room mRoom;
    private int mSelection = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            mRoom = Room.fromJson(Room.MOCK);
            mRoomView = new RoomView(this, mRoom);
            setContentView(mRoomView);
        } catch (JSONException e) {
            e.printStackTrace();
            setContentView(R.layout.activity_room);
        }

        mTextView = (TextView) findViewById(R.id.text);

        // Enables Always-on
        setAmbientEnabled();

        mRoomView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelection = (mSelection + 1) % (mRoom.gadgets.size());
                mRoomView.selectDevice(mSelection);
            }
        });
    }
}
