# Sensor data serialization

To pass around sensor data, it is serialized as a protobuf message. The messages
are defined in the sensor.proto definition file

## Coordinate System

The standard coordinate system for the acceleration sensors is given relative 
to the users wrist as pictured in the following image. The z-axis points 
towards out of the screen towards the reader (or away from the skin if 
expressed relative to the wrist).

![](coordinate_system.png "Coordinate Sytem")

The coordinate system for the gyroscope is the same as for the accelerometer.

Rotation is defined mathematical positive. That is, an observer
looking from some positive location on the x, y or z axis at the device
positioned on the origin would report positive rotation if the device appeared
to be rotating counter clockwise. (adapted from https://developer.android.com/
guide/topics/sensors/sensors_motion.html)

## Units

* Acceleration: milli g
* Angular rate: rad/s
* Magnetometer: micro Tesla