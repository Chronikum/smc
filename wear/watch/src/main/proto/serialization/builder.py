import kinemic as kn
import sensor_pb2
import pandas as pd

ACC = ["acc_x", "acc_y", "acc_z"]
GYRO = []

def f2i(f):
    """
    Savely convert a float to an integer.

    If the float does not represent an integer, a ValueError is raised.

    :param f: float to be converted
    :return: the given float as int
    :raise: ValueError if float is not a valid int
    """
    if not f.is_integer():
        raise ValueError("{} is not an int".format(f))
    return int(f)

#TODO: This whole thing is currently underspecified as battery, pressure and
#  timestamps have no units, must be fully specified and unit tested
def from_data_frame(dframe, sampling_rate = None):
    """
    Construct a protobuf message from DateFrame and return it

    The data in the DataFrame is expected to be in SI units, where

    acceleration is given in milli g
    angular rate is given in rad/s
    magnetic field strength is given in micro T

    All data except quaternions must be given as integers.

    :param dframe: DataFrame, the column indices must match the protobuf field
        names. In case of subfields, "_" replaces the "." used for accessing
        the subfield in the column name (e.g. acc.x -> "acc_x")
    :param sampling_rate: sampling rate in Hz, not mandatory as field is optional
    :raise ValueError in case dframe has no rows or is None
    :raise KeyError if mandatory ACC and GYRO columns are not present
    :return: instance of protobuf message
    """
    if dframe is None:
        raise ValueError("DataFrame cannot be None")
    if dframe.shape[0] == 0:
        raise ValueError("DataFrame contains no data")
    packet = sensor_pb2.SensorPacket()
    for index, row in dframe.iterrows():
        valpack = packet.values.add()
        valpack.acc.x = row[kn.ACC_X]
        valpack.acc.y = row[kn.ACC_Y]
        valpack.acc.z = row[kn.ACC_Z]
        valpack.gyro.x = row[kn.GYRO_X]
        valpack.gyro.y = row[kn.GYRO_Y]
        valpack.gyro.z = row[kn.GYRO_Z]
        if set([kn.MAG_X, kn.MAG_Y, kn.MAG_Z]).issubset(dframe.columns):
            if row[[kn.MAG_X, kn.MAG_Y, kn.MAG_Z]].notnull().all():
                valpack.mag.x = row[kn.MAG_X]
                valpack.mag.y = row[kn.MAG_Y]
                valpack.mag.z = row[kn.MAG_Z]
        if set([kn.Q_X, kn.Q_X, kn.Q_Y, kn.Q_Z]).issubset(dframe.columns):
            if row[[kn.Q_W, kn.Q_X, kn.Q_Y, kn.Q_Z]].notnull().all():
                valpack.q.w = row[kn.Q_W]
                valpack.q.x = row[kn.Q_X]
                valpack.q.y = row[kn.Q_Y]
                valpack.q.z = row[kn.Q_Z]
        if kn.BATTERY in dframe and not pd.isnull(row[kn.BATTERY]):
            valpack.battery = int(row[kn.BATTERY])
        if kn.TEMPERATURE in dframe and not pd.isnull(row[kn.TEMPERATURE]):
            valpack.temperature = int(row[kn.TEMPERATURE])
        if kn.PRESSURE in dframe and not pd.isnull(row[kn.PRESSURE]):
            valpack.pressure = int(row[kn.PRESSURE])
        if kn.TIMESTAMP in dframe and not pd.isnull(row[kn.TIMESTAMP]):
            valpack.timestamp = int(row[kn.TIMESTAMP])
    packet.metadata.name = "Simulator"
    if sampling_rate is not None:
        packet.metadata.sampling_rate = sampling_rate
    return packet
