# hunter configurations for biokit dependencies
if (ANDROID OR IOS OR RASPBERRY)
  hunter_config(Protobuf
      VERSION 3.3.0}
      CMAKE_ARGS
          protobuf_BUILD_LIBPROTOBUF_LITE=OFF
          protobuf_BUILD_TESTS=OFF
          protobuf_BUILD_PROTOC=OFF
      CONFIGURATION_TYPES Release
      )
else()
  hunter_config(Protobuf
      VERSION 3.3.0
      CMAKE_ARGS
          protobuf_BUILD_LIBPROTOBUF_LITE=OFF
      CONFIGURATION_TYPES Release
      )
endif()