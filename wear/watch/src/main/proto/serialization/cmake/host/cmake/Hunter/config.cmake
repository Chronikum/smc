# hunter config for 'host' projects
hunter_config(Protobuf # we only need protoc in release mode for code generation
        VERSION 3.3.0
        CMAKE_ARGS
            protobuf_BUILD_LIBPROTOBUF_LITE=OFF
        CONFIGURATION_TYPES Release
)