#!/bin/sh
if [ $# -eq 0 ] 
then
    echo "no protocol buffer compiler given, use system protoc if present"
    echo "a specific protocol buffers compiler can be passed as command line argument"
    PROTOC=protoc
else
    PROTOC=$1
fi
echo "use protocol buffers compiler: $PROTOC"
$PROTOC --python_out=. *.proto
