package de.kinemic.wear.recorder.service;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.util.SparseIntArray;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import static android.content.Context.POWER_SERVICE;


/**
 * Created by Fabian on 04.12.16.
 */

public class InternalSensorRecorder implements Recorder {
    private static final String TAG = "InternalSensorRecorder";
    private static final String TAG_REPORT = "Recorder_Report";

    private final DateFormat mFilenameDateFormat = new SimpleDateFormat("yyyy-MM-dd--HH-mm", Locale.getDefault());
    private final DateFormat mDurationFormat = new SimpleDateFormat("HH:mm:ss", Locale.getDefault());

    private static final int SENSOR_DELAY_US = SensorManager.SENSOR_DELAY_GAME;
    private static final int SENSOR_MAX_BATCH_DELAY_US = 1000000; // 1s;
    private static final long REPORT_DELAY_MS = 5 * 60000L; // 5min

    // accessed from the main thread
    private @RecorderState int mState = STATE_IDLE;
    private SparseIntArray mCounts;
    private ArrayList<OnStateChangeListener> mStateListeners;
    private SaveResultHandler mSaveResultHandler;
    private SensorManager mSensorManager;
    private PowerManager.WakeLock mWakeLock;

    // accessed from the io thread
    private File mTmpFile;
    private BufferedOutputStream mOutStream;
    private Date mStartDate, mEndDate;

    private Context mContext;
    private HandlerThread mIOThread;
    private Handler mIOHandler;
    private Handler mMainHandler;

    private Alarm mAlarm = new Alarm();

    public InternalSensorRecorder(Context context) {
        mContext = context;
        mStateListeners = new ArrayList<>();
        mCounts = new SparseIntArray();

        mIOThread = new HandlerThread("RecorderIO");
        mIOThread.start();
        mIOHandler = new Handler(mIOThread.getLooper());
        mMainHandler = new Handler(Looper.getMainLooper());

        /* mIOHandler.post(new Runnable() {
            @Override
            public void run() {
                migrateOldFolder();
            }
        }); */

        mIOHandler.postDelayed(mReportSamplesRunner, REPORT_DELAY_MS);

        PowerManager powerManager = (PowerManager) context.getSystemService(POWER_SERVICE);
        mWakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);
    }

    public void quit() {
        stop();
        mIOHandler.removeCallbacks(mReportSamplesRunner);
        mIOThread.quitSafely();
    }

    private final Runnable mReportSamplesRunner = new Runnable() {
        @Override
        public void run() {
            if (mState == STATE_RECORDING) {
                long duration = (new Date()).getTime() - mStartDate.getTime();
                Log.d(TAG_REPORT, "Duration: " + mDurationFormat.format(new Date(duration - TimeZone.getDefault().getRawOffset())));
                for (int i = 0; i < mCounts.size(); ++i) {
                    Log.d(TAG_REPORT, "Sensor " + mCounts.keyAt(i) + ": " + mCounts.valueAt(i) + " samples (" + (1000.f * mCounts.valueAt(i) / duration) + "Hz)");
                }
            }
            mIOHandler.postDelayed(this, REPORT_DELAY_MS);
        }
    };


    @Override
    public void start() {
        if (mState == STATE_STOPPED) {
            if (Log.isLoggable(TAG, Log.WARN)) {
                Log.w(TAG, "called start without discarding a stopped record. Is this really what you want?");
            }
            discard();
        }

        if (mState == STATE_IDLE) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "Start to record");
            }
            mStartDate = new Date();
            mIOHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        File dir = new File(mContext.getExternalFilesDir(null), "records");
                        mTmpFile = new File(dir, mFilenameDateFormat.format(mStartDate) + "--" +  android.os.Build.MODEL.replace(" ", "_") + ".tmp.wr");
                        mTmpFile.getParentFile().mkdirs();
                        Log.d(TAG, "created temporary record: " + mTmpFile);
                        // mTmpFile = File.createTempFile("record", "wr", mContext.getCacheDir());
                        mOutStream = new BufferedOutputStream(new FileOutputStream(mTmpFile, false));

                        mCounts.clear();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            startListening();
            mWakeLock.acquire();
            mAlarm.setAlarm(mContext);
            setState(STATE_RECORDING);
        }
    }

    @Override
    public void stop() {
        if (mState == STATE_RECORDING) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "Stop to record");
            }

            mIOHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        mOutStream.close();
                        mOutStream = null;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });
            mEndDate = new Date();
            stopListening();
            mWakeLock.release();
            mAlarm.cancelAlarm(mContext);
            setState(STATE_STOPPED);
        }
    }

    @Override
    public void cancel() {
        stop();
        discard();
    }

    @Override
    public void discard() {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "Discard record.");
        }
        if (mState == STATE_STOPPED) {
            mIOHandler.post(new Runnable() {
                @Override
                public void run() {
                    mTmpFile.delete();
                }
            });
            setState(STATE_IDLE);
        }
    }

    @Override
    public void save() {
        save(null);
    }

    private void migrateOldFolder() {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "migrate files from old directories");
        }

        File dir = new File(mContext.getExternalFilesDir(null), "records");
        dir.mkdirs();
        for (File f : (new File(mContext.getFilesDir(), "records")).listFiles()) {
            File outFile = new File(dir, f.getName());
            try {
                copyFile(f, outFile);
                if (Log.isLoggable(TAG, Log.DEBUG)) {
                    Log.d(TAG, "copied old record from: " + f.getAbsolutePath()+ " to: " + outFile.getAbsolutePath());
                }
                f.deleteOnExit();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void save(@Nullable final SaveResultHandler resultHandler) {
        if (mState == STATE_RECORDING) stop(); // need to stop here too for end timestamp

        if (mState == STATE_STOPPED) {
            File dir = new File(mContext.getExternalFilesDir(null), "records");
            File ouput = new File(dir, mFilenameDateFormat.format(mEndDate) + "--" +  android.os.Build.MODEL.replace(" ", "_") + ".wr");
            save(ouput, resultHandler);
        }
    }

    @Override
    public void save(final @NonNull File file, final @Nullable SaveResultHandler resultHandler) {
        if (mState == STATE_RECORDING) stop();

        if (mState == STATE_STOPPED) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "Save record to: " + file.getAbsolutePath());
            }

            mIOHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        file.getParentFile().mkdirs();
                        copyFile(mTmpFile, file);
                        mTmpFile.delete();

                        if (Log.isLoggable(TAG, Log.DEBUG)) {
                            Log.d(TAG, "Successfully saved record to: " + file.getAbsolutePath());
                            long duration = mEndDate.getTime() - mStartDate.getTime();
                            Log.d(TAG, "Duration: " + mDurationFormat.format(new Date(duration - TimeZone.getDefault().getRawOffset())));
                            for (int i = 0; i < mCounts.size(); ++i) {
                                Log.d(TAG, "Sensor " + mCounts.keyAt(i) + ": " + mCounts.valueAt(i) + " samples (" + (1000.f * mCounts.valueAt(i) / duration) + "Hz)");
                            }
                        }

                        if (resultHandler != null ) {
                            mMainHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    resultHandler.onRecordSaveSuccess(file);
                                }
                            });
                        }
                    } catch (final IOException e) {
                        e.printStackTrace();
                        if (resultHandler != null) {
                            mMainHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    resultHandler.onRecordSaveFailed(file, e);
                                }
                            });
                        }
                    }
                }
            });
            setState(STATE_IDLE);
        }
    }

    @Override
    public void registerOnStateChangeListener(@NonNull OnStateChangeListener listener) {
        if (!mStateListeners.contains(listener)) {
            mStateListeners.add(listener);
            //listener.onRecorderStateChange(mState);
        }
    }

    @Override
    public void unregisterOnStateChangeListener(@NonNull OnStateChangeListener listener) {
        if (mStateListeners.contains(listener)) {
            mStateListeners.remove(listener);
        }
    }

    @Override
    public int getState() {
        return mState;
    }

    @Override
    public Date getRecordStartTime() {
        if (mStartDate == null) return null;
        return new Date(mStartDate.getTime());
    }

    private void setState(@RecorderState int state) {
        if (mState != state) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "changed state from " + mState + " to " + state);
            }

            mState = state;
            for (OnStateChangeListener listener : mStateListeners) {
                listener.onRecorderStateChange(state);
            }
        }
    }

    private static void copyFile(File src, File dst) throws IOException
    {
        FileChannel inChannel = new FileInputStream(src).getChannel();
        FileChannel outChannel = new FileOutputStream(dst).getChannel();
        try
        {
            inChannel.transferTo(0, inChannel.size(), outChannel);
        }
        finally
        {
            if (inChannel != null) inChannel.close();
            if (outChannel != null) outChannel.close();
        }
    }


    private void startListening() {
        mSensorManager = ((SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE));

        /*if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "Scanning Sensors...");
            List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
            Log.d(TAG, "Found " + sensors.size() + " sensors:");
            for (Sensor s : sensors) {
                Log.d(TAG, s.toString());
            }
        }*/

        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "Start to listen");
        }

        Sensor accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (Log.isLoggable(TAG, Log.INFO)) {
            Log.i(TAG, "Accelerometer fifo size: " + accelerometer.getFifoMaxEventCount());
        }
        Sensor gyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE_UNCALIBRATED);
        if (gyroscope == null) {
            if (Log.isLoggable(TAG, Log.INFO)) {
                Log.i(TAG, "No sensor of type: 'gyroscope_uncalibrated'. Fall back to 'gyroscope'");
            }
            gyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
            if (Log.isLoggable(TAG, Log.INFO)) {
                Log.i(TAG, "Gyroscope fifo size: " + gyroscope.getFifoMaxEventCount());
            }
        }
        Sensor magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);
        if (magnetometer == null) {
            if (Log.isLoggable(TAG, Log.INFO)) {
                Log.i(TAG, "No sensor of type: 'magnetometer_uncalibrated'. Fall back to 'magnetometer'");
            }
            magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
            if (magnetometer == null) {
                if (Log.isLoggable(TAG, Log.INFO)) {
                    Log.i(TAG, "No sensor of type: 'magnetometer'.");
                }
            } else {
                if (Log.isLoggable(TAG, Log.INFO)) {
                    Log.i(TAG, "Magnetometer fifo size: " + magnetometer.getFifoMaxEventCount());
                }
            }
        }

        mSensorManager.registerListener(mSensorListener, accelerometer, SENSOR_DELAY_US, SENSOR_MAX_BATCH_DELAY_US);
        mSensorManager.registerListener(mSensorListener, gyroscope, SENSOR_DELAY_US, SENSOR_MAX_BATCH_DELAY_US);
        if (magnetometer != null) mSensorManager.registerListener(mSensorListener, magnetometer, SensorManager.SENSOR_DELAY_GAME, SENSOR_MAX_BATCH_DELAY_US);
    }

    private void stopListening() {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "Stop to listen");
        }
        if (mSensorManager != null) {
            mSensorManager.unregisterListener(mSensorListener);
        }
    }

    private final SensorEventListener mSensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            if (mState == STATE_RECORDING) {
                final WearSample sample = new WearSample(event);

                mIOHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            mOutStream.write(sample.getBytes());
                            int count = mCounts.get(sample.type, 0);
                            mCounts.put(sample.type, count + 1);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, sensor.getStringType() + " changed accuracy to: " + accuracy);
            }
        }
    };
}
