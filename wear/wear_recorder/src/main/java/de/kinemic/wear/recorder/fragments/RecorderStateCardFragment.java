package de.kinemic.wear.recorder.fragments;

import android.os.Bundle;
import android.support.wearable.view.CardFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import de.kinemic.wear.recorder.R;

/**
 * Created by Fabian on 05.12.16.
 */

public class RecorderStateCardFragment extends CardFragment {

    public static RecorderStateCardFragment create() {
        RecorderStateCardFragment fragment = new RecorderStateCardFragment();
        return fragment;
    }

    @Override
    public View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_frame_placeholder, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        getChildFragmentManager().beginTransaction().replace(R.id.container, RecorderStateFragment.create()).commit();
    }
}
