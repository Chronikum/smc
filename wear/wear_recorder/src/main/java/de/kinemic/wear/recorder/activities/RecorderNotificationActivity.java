package de.kinemic.wear.recorder.activities;

import android.app.Activity;
import android.os.Bundle;

import de.kinemic.wear.recorder.R;
import de.kinemic.wear.recorder.fragments.RecorderStateFragment;

/**
 * Created by Fabian on 04.12.16.
 */

public class RecorderNotificationActivity extends Activity {

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_frame_placeholder);
        getFragmentManager().beginTransaction()
                .replace(R.id.container, RecorderStateFragment.create()).commit();
    }
}
