package de.kinemic.wear.recorder.fragments;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.util.Log;

import java.io.File;

import de.kinemic.wear.recorder.service.Recorder;
import de.kinemic.wear.recorder.service.RecorderService;

public class RecorderBaseFragment extends Fragment implements Recorder.OnStateChangeListener, Recorder.SaveResultHandler {
    private static final String TAG = "RecorderBaseFragment";

    protected Recorder mRecorder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onAttach()");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onDetach()");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onResume()");
        }
        getActivity().bindService(new Intent(getActivity(), RecorderService.class), mRecorderServiceConnection, 0);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onPause()");
        }
        if (mRecorder != null) mRecorder.unregisterOnStateChangeListener(this);
        getActivity().unbindService(mRecorderServiceConnection);
        mRecorder = null;
    }

    private ServiceConnection mRecorderServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "onServiceConnected()");
            }
            mRecorder = ((RecorderService.LocalBinder) service).getRecorder();
            mRecorder.registerOnStateChangeListener(RecorderBaseFragment.this);
            onConnected();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "onServiceDisconnected()");
            }
            mRecorder = null;
            onDisconnected();
        }
    };


    protected void onConnected() {

    }

    protected void onDisconnected() {

    }

    @Override
    public void onRecorderStateChange(@Recorder.RecorderState int state) {
        if (Log.isLoggable(TAG, Log.DEBUG)) {
            Log.d(TAG, "onRecorderStateChange( " + state + " )");
        }
    }

    @Override
    public void onRecordSaveSuccess(@NonNull File record) {

    }

    @Override
    public void onRecordSaveFailed(@NonNull File record, @NonNull Exception exception) {

    }
}
