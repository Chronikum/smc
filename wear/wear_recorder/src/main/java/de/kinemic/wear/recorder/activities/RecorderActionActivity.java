package de.kinemic.wear.recorder.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.wearable.view.ConfirmationOverlay;
import android.util.Log;

import java.io.File;

import de.kinemic.wear.recorder.service.Recorder;

public class RecorderActionActivity extends RecorderBaseActivity implements ConfirmationOverlay.FinishedAnimationListener {

    public static final String EXTRA_ACTION = "action";
    public static final int ACTION_START = 1;
    public static final int ACTION_STOP = 2;
    public static final int ACTION_SAVE = 3;
    public static final int ACTION_DISCARD = 4;
    public static final int ACTION_CANCEL = 5;

    private static final long SERVICE_TIMEOUT_MS = 1000L;
    private static final long ACTION_TIMEOUT_MS = 10000L;

    private int mAction;
    private Handler mHandler;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setTheme(android.support.wearable.R.style.ConfirmationActivity);

        mAction = getIntent().getIntExtra(EXTRA_ACTION, 0);
        Log.d("RecordActionActivity", "got action: " + mAction);
        if (mAction <= 0) throw new IllegalArgumentException("EXTRA_ACTION must be set to a proper value");

        mHandler = new Handler();
        mHandler.postDelayed(mServiceTimeout, SERVICE_TIMEOUT_MS);
    }

    private Runnable mServiceTimeout = new Runnable() {
        @Override
        public void run() {
            setResult(RESULT_CANCELED);
            showOverlay(ConfirmationOverlay.FAILURE_ANIMATION, "No Service");
        }
    };

    private Runnable mActionTimeout = new Runnable() {
        @Override
        public void run() {
            setResult(RESULT_CANCELED);
            showOverlay(ConfirmationOverlay.FAILURE_ANIMATION, "Failed (timeout)");
        }
    };

    private void showOverlay(int type, String message) {
        (new ConfirmationOverlay()).setType(type).setMessage(message).setFinishedAnimationListener(this).showOn(this);
    }

    public void onAnimationFinished() {
        this.finish();
    }

    protected void onConnected() {
        super.onConnected();
        Log.d("RecordActionActivity", "onConnect action: " + mAction);

        mHandler.removeCallbacks(mServiceTimeout);

        switch (mAction) {
            case ACTION_START:
                mRecorder.start();
                break;
            case ACTION_STOP:
                mRecorder.stop();
                break;
            case ACTION_DISCARD:
                mRecorder.discard();
                break;
            case ACTION_SAVE:
                mRecorder.save(this);
                break;
            case ACTION_CANCEL:
                mRecorder.cancel();
                break;
        }

        mHandler.postDelayed(mActionTimeout, ACTION_TIMEOUT_MS);
    }

    public void onRecordSaveSuccess(@NonNull File file) {
        super.onRecordSaveSuccess(file);
        if (mAction == ACTION_SAVE) {
            setResult(RESULT_OK);
            showOverlay(ConfirmationOverlay.SUCCESS_ANIMATION, "Saved");
        }
    }

    public void onRecordSaveFailed(@NonNull File file, @NonNull Exception e) {
        super.onRecordSaveFailed(file, e);
        if (mAction == ACTION_SAVE) {
            setResult(RESULT_CANCELED);
            showOverlay(ConfirmationOverlay.FAILURE_ANIMATION, "Save Failed");
        }
    }

    public void onRecorderStateChange(int state) {
        super.onRecorderStateChange(state);

        switch (mAction) {
            case ACTION_START:
                if (state == Recorder.STATE_RECORDING) {
                    setResult(RESULT_OK);
                    showOverlay(ConfirmationOverlay.SUCCESS_ANIMATION, "Started");
                }
                break;
            case ACTION_STOP:
                if (state == Recorder.STATE_STOPPED) {
                    setResult(RESULT_OK);
                    showOverlay(ConfirmationOverlay.SUCCESS_ANIMATION, "Stopped");
                }
                break;
            case ACTION_DISCARD:
                if (state == Recorder.STATE_IDLE) {
                    setResult(RESULT_OK);
                    showOverlay(ConfirmationOverlay.SUCCESS_ANIMATION, "Discarded");
                }
                break;
            case ACTION_CANCEL:
                if (state == Recorder.STATE_IDLE) {
                    setResult(RESULT_OK);
                    showOverlay(ConfirmationOverlay.SUCCESS_ANIMATION, "Canceled");
                }
                break;
        }
    }

}
