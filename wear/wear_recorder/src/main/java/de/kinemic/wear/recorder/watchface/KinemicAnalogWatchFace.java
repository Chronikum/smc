/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.kinemic.wear.recorder.watchface;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.util.Log;
import android.util.Property;
import android.view.SurfaceHolder;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Wearable;

import java.lang.ref.WeakReference;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import de.kinemic.wear.recorder.R;
import de.kinemic.wear.recorder.activities.RecorderActionActivity;
import de.kinemic.wear.recorder.service.Recorder;
import de.kinemic.wear.recorder.activities.RecorderActivity;
import de.kinemic.wear.recorder.service.RecorderService;

import static java.lang.Math.abs;

/**
 * Analog watch face with a ticking second hand. In ambient mode, the second hand isn't
 * shown. On devices with low-bit ambient mode, the hands are drawn without anti-aliasing in ambient
 * mode. The watch face is drawn with less contrast in mute mode.
 */
public class KinemicAnalogWatchFace extends CanvasWatchFaceService {

    /*
     * Update rate in milliseconds for interactive mode. We update once a second to advance the
     * second hand.
     */
    private static final long INTERACTIVE_UPDATE_RATE_MS = TimeUnit.SECONDS.toMillis(1);

    /**
     * Handler message id for updating the time periodically in interactive mode.
     */
    private static final int MSG_UPDATE_TIME = 0;

    @Override
    public Engine onCreateEngine() {
        return new Engine();
    }

    private static class EngineHandler extends Handler {
        private final WeakReference<KinemicAnalogWatchFace.Engine> mWeakReference;

        public EngineHandler(KinemicAnalogWatchFace.Engine reference) {
            mWeakReference = new WeakReference<>(reference);
        }

        @Override
        public void handleMessage(Message msg) {
            KinemicAnalogWatchFace.Engine engine = mWeakReference.get();
            if (engine != null) {
                switch (msg.what) {
                    case MSG_UPDATE_TIME:
                        engine.handleUpdateTimeMessage();
                        break;
                }
            }
        }
    }

    private class Engine extends CanvasWatchFaceService.Engine implements DataApi.DataListener,
            GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
        private static final String TAG = "KinemicWFEngine";

        private static final float TOTAL_FACE = 0.906f;
        private static final float HOUR_STROKE_WIDTH = 10f * TOTAL_FACE;
        private static final float MINUTE_STROKE_WIDTH = 10f * TOTAL_FACE;
        private static final float SECOND_TICK_STROKE_WIDTH = 1f * TOTAL_FACE;

        private static final float CENTER_GAP_AND_CIRCLE_RADIUS = 10f * TOTAL_FACE;

        private static final int SHADOW_RADIUS = 6;
        private final Rect mPeekCardBounds = new Rect();
        /* Handler to update the time once a second in interactive mode. */
        private final Handler mUpdateTimeHandler = new EngineHandler(this);
        private Calendar mCalendar;
        private final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mCalendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            }
        };
        private boolean mRegisteredTimeZoneReceiver = false;
        private boolean mMuteMode;
        private float mCenterX;
        private float mCenterY;
        private float mSecondHandLength;
        private float sMinuteHandLength;
        private float sHourHandLength;
        /* Colors for all hands (hour, minute, seconds, ticks) based on photo loaded. */
        private int mWatchHandColor;
        private int mWatchHandHighlightColor;
        private int mWatchHandShadowColor;
        private int mRecordingColor;
        private int mBackgroundColor;
        private Paint mHourPaint;
        private Paint mMinutePaint;
        private Paint mSecondPaint;
        private Paint mTickAndCirclePaint;
        private Paint mInnerCirclePaint;
        private Paint mBackgroundPaint;
        private Paint mRecordingPaint;
        //private Bitmap mBackgroundBitmap;
        //private Bitmap mGrayBackgroundBitmap;
        private Bitmap mGradiantHourBitmap;
        private Bitmap mGradiantMinuteBitmap;
        private Bitmap mGradiantSecondBitmap;
        private Bitmap mGradiantRecordBitmap;
        private Bitmap mIconBitmap;
        private Bitmap mRecordBitmap;
        private Bitmap mSaveBitmap;
        private boolean mAmbient;
        private boolean mLowBitAmbient;
        private boolean mBurnInProtection;

        private static final float HOUR_HAND_LENGTH = 0.75f * TOTAL_FACE;
        private static final float MINUTE_HAND_LENGTH = 0.93f * TOTAL_FACE;
        private static final float SECOND_HAND_LENGTH = 1.0f * TOTAL_FACE;
        private static final float WATCH_FACE_RADIUS = 0.5f * TOTAL_FACE;
        private static final float WATCH_FACE_DRAWER_RADIUS = 0.42f * TOTAL_FACE;
        private static final float RECORD_INNER_RADIUS = 0.33f * TOTAL_FACE;

        private float mMarginTop;
        private float mMarginRight;
        private float mIconHeiht;
        private float mTextSizeSmall;
        private float mMarginText;
        private float mMarginTextBottom;
        private Rect mTextBounds = new Rect();

        private boolean mRecording = false;
        private long mRecordBase;

        private final DateFormat mDiffFormat = new SimpleDateFormat("H:mm", Locale.getDefault());
        private final DateFormat mAmbientDiffFormat = new SimpleDateFormat("H:mm", Locale.getDefault());
        private Paint mTextPaint;
        private Paint.FontMetrics mTextFontMetrics = new Paint.FontMetrics();

        private float mMiddleDrawerProgress;
        private ObjectAnimator mDrawerAnimator;
        private static final long DRAWER_OPEN_DURATION = 500L;
        private boolean mDrawerIsOpen = false;

        private Bitmap mStaticBackground;
        private Bitmap mMinuteBackground;
        private Paint mBitmapPaint;
        private int mLastMinuteUpdate;

        private Recorder mRecorder;
        private int mRecorderState = -1;

        GoogleApiClient mGoogleApiClient = new GoogleApiClient.Builder(KinemicAnalogWatchFace.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wearable.API)
                .build();

        /*
        * Animations
        */
        private Property<Engine, Float> radiusProperty
                = new Property<Engine, Float>(Float.class, "drawerProgress") {
            @Override
            public Float get(Engine object) {
                return object.getDrawerProgress();
            }

            @Override
            public void set(Engine object, Float value) {
                object.setDrawerProgress(value);
            }
        };

        private float getDrawerProgress() {
            return mMiddleDrawerProgress;
        }


        public void setDrawerProgress(float progress) {
            this.mMiddleDrawerProgress = progress;
            invalidate();
        }


        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);

            setWatchFaceStyle(new WatchFaceStyle.Builder(KinemicAnalogWatchFace.this)
                    .setCardPeekMode(WatchFaceStyle.PEEK_MODE_SHORT)
                    .setBackgroundVisibility(WatchFaceStyle.BACKGROUND_VISIBILITY_INTERRUPTIVE)
                    .setShowSystemUiTime(false)
                    .setAcceptsTapEvents(true)
                    .build());

            mBackgroundColor = getColor(R.color.kinemic_gray);
            mBackgroundPaint = new Paint();
            mBackgroundPaint.setColor(mBackgroundColor);

            mGradiantHourBitmap = getCircleBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.gradiant_degree_alpha), HOUR_HAND_LENGTH);
            mGradiantMinuteBitmap = getCircleBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.gradiant_degree_alpha), MINUTE_HAND_LENGTH);
            mGradiantSecondBitmap = getCircleBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.gradiant_degree_alpha), SECOND_HAND_LENGTH);
            mGradiantRecordBitmap = getCircleBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.gradiant_degree_alpha), WATCH_FACE_DRAWER_RADIUS);
            mIconBitmap = getBitmap(getApplicationContext(), R.drawable.ic_kinemic_icon_24_white);
            mRecordBitmap = getBitmap(getApplicationContext(), R.drawable.ic_52_record_rec_white);
            mSaveBitmap = getBitmap(getApplicationContext(), R.drawable.ic_52_save_white);

            mMarginRight = getResources().getDimension(R.dimen.margin_wf_right);
            mMarginTop = getResources().getDimension(R.dimen.margin_wf_top);
            mIconHeiht = getResources().getDimension(R.dimen.wf_icon_height);
            mTextSizeSmall = getResources().getDimension(R.dimen.wf_text_size_duration);
            mMarginText = getResources().getDimension(R.dimen.wf_margin_text_top);
            mMarginTextBottom = getResources().getDimension(R.dimen.wf_margin_text_bottom);

            mIconBitmap = Bitmap.createScaledBitmap(mIconBitmap, (int) mIconHeiht, (int) mIconHeiht, true);

            /* Set defaults for colors */
            mWatchHandColor = Color.WHITE;
            mWatchHandHighlightColor = Color.WHITE;
            mWatchHandShadowColor = Color.BLACK;
            mRecordingColor = Color.parseColor("#FFFF1111");



            mHourPaint = new Paint();
            mHourPaint.setColor(mWatchHandColor);
            mHourPaint.setStrokeWidth(HOUR_STROKE_WIDTH);
            mHourPaint.setAntiAlias(true);
            mHourPaint.setStrokeCap(Paint.Cap.SQUARE);

            mMinutePaint = new Paint();
            mMinutePaint.setColor(mWatchHandColor);
            mMinutePaint.setStrokeWidth(MINUTE_STROKE_WIDTH);
            mMinutePaint.setAntiAlias(true);
            mMinutePaint.setStrokeCap(Paint.Cap.SQUARE);

            mSecondPaint = new Paint();
            mSecondPaint.setColor(mWatchHandHighlightColor);
            mSecondPaint.setStrokeWidth(SECOND_TICK_STROKE_WIDTH);
            mSecondPaint.setAntiAlias(true);
            mSecondPaint.setStrokeCap(Paint.Cap.SQUARE);

            mTickAndCirclePaint = new Paint();
            mTickAndCirclePaint.setColor(mWatchHandColor);
            mTickAndCirclePaint.setStrokeWidth(SECOND_TICK_STROKE_WIDTH);
            mTickAndCirclePaint.setAntiAlias(true);
            mTickAndCirclePaint.setStyle(Paint.Style.FILL_AND_STROKE);

            mInnerCirclePaint = new Paint();
            mInnerCirclePaint.setColor(mWatchHandColor);
            mInnerCirclePaint.setStrokeWidth(CENTER_GAP_AND_CIRCLE_RADIUS * 0.5f);
            mInnerCirclePaint.setAntiAlias(true);
            mInnerCirclePaint.setStyle(Paint.Style.STROKE);

            mRecordingPaint = new Paint();
            mRecordingPaint.setColor(mRecordingColor);
            mRecordingPaint.setStrokeWidth(SECOND_TICK_STROKE_WIDTH);
            mRecordingPaint.setStyle(Paint.Style.FILL);
            mRecordingPaint.setAntiAlias(true);

            mCalendar = Calendar.getInstance();

            mRecordBase = mCalendar.getTime().getTime();
            mTextPaint = createTextPaint(Color.WHITE, Typeface.create("sans-serif-light", Typeface.NORMAL));
            mTextPaint.setTextSize(mTextSizeSmall);
            mTextPaint.setAntiAlias(true);
            mTextPaint.getTextBounds("00:00:59", 0, 8, mTextBounds);
            mTextPaint.getFontMetrics(mTextFontMetrics);

            mMiddleDrawerProgress = 0.f;

            mBitmapPaint = new Paint();
            mBitmapPaint.setAntiAlias(false);

            mStaticBackground = Bitmap.createBitmap(320, 320, Bitmap.Config.ARGB_8888);
            mMinuteBackground = Bitmap.createBitmap(320, 320, Bitmap.Config.ARGB_8888);
            mLastMinuteUpdate = -1;
        }

        @TargetApi(Build.VERSION_CODES.LOLLIPOP)
        private Bitmap getBitmap(VectorDrawable vectorDrawable) {
            Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                    vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            vectorDrawable.draw(canvas);
            return bitmap;
        }

        private Bitmap getBitmap(Context context, int drawableId) {
            Drawable drawable = ContextCompat.getDrawable(context, drawableId);
            if (drawable instanceof BitmapDrawable) {
                return ((BitmapDrawable) drawable).getBitmap();
            } else if (drawable instanceof VectorDrawable) {
                return getBitmap((VectorDrawable) drawable);
            } else {
                throw new IllegalArgumentException("unsupported drawable type");
            }
        }

        private void updateStaticBackground() {
            Canvas canvas = new Canvas(mStaticBackground);

            if (mAmbient && (mLowBitAmbient || mBurnInProtection)) {
                canvas.drawColor(Color.BLACK);
            } else if (mAmbient) {
                //canvas.drawBitmap(mGreyScaleBackground, 0, 0, mBackgroundPaint);
                canvas.drawPaint(mBackgroundPaint);
            } else {
                //canvas.drawBitmap(mBackgroundBitmap, 0, 0, mBackgroundPaint);
                canvas.drawPaint(mBackgroundPaint);
            }
        }

        private void updateMinuteBackground(int width, int height) {
            mMinuteBackground.recycle();
            mMinuteBackground = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            updateMinuteBackground();
        }

        private void updateMinuteBackground() {
            //Log.d("WATCH", "update minute background");
            Canvas canvas = new Canvas(mMinuteBackground);

            /* draw static background */
            canvas.drawBitmap(mStaticBackground, 0, 0, mBitmapPaint);

            final float seconds = (mCalendar.get(Calendar.SECOND) + mCalendar.get(Calendar.MILLISECOND) / 1000f);
            final float secondsRotation = seconds * 6f;
            final float minutesRotation = mCalendar.get(Calendar.MINUTE) * 6f;
            final float hourHandOffset = mCalendar.get(Calendar.MINUTE) / 2f;
            final float hoursRotation = (mCalendar.get(Calendar.HOUR) * 30) + hourHandOffset;

            /* draw dynamic watch face at minute level */
            if (!mAmbient) {
                canvas.save();

                canvas.rotate(hoursRotation, mCenterX, mCenterY);
                canvas.drawBitmap(mGradiantHourBitmap, 0, 0, mBackgroundPaint);

                canvas.rotate(minutesRotation - hoursRotation, mCenterX, mCenterY);
                canvas.drawBitmap(mGradiantMinuteBitmap, 0, 0, mBackgroundPaint);

                canvas.restore();
            }

            /* kinemic icon */
            if (!mAmbient) {
                canvas.drawBitmap(mIconBitmap, 2.0f*mCenterX - mMarginRight - mIconHeiht, mMarginTop, mBackgroundPaint);
            }
        }

        private void updateStaticBackground(int width, int height) {
            mStaticBackground.recycle();
            mStaticBackground = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            updateStaticBackground();
        }

        private void openMiddleDrawer(boolean animated) {
            if (mDrawerAnimator != null) {
                mDrawerAnimator.cancel();
                mDrawerAnimator.removeAllListeners();
            }

            if (animated) {
                mDrawerAnimator = ObjectAnimator.ofFloat(this, radiusProperty, mMiddleDrawerProgress, 1.0f);
                mDrawerAnimator.setDuration(DRAWER_OPEN_DURATION);
                mDrawerAnimator.setInterpolator(new DecelerateInterpolator());

                mDrawerAnimator.start();
            } else {
                mMiddleDrawerProgress = 1.f;
                invalidate();
            }
        }

        private void closeMiddleDrawer(boolean animated) {
            if (mDrawerAnimator != null) {
                mDrawerAnimator.cancel();
                mDrawerAnimator.removeAllListeners();
            }

            if (animated) {
                mDrawerAnimator = ObjectAnimator.ofFloat(this, radiusProperty, mMiddleDrawerProgress, 0.0f);
                mDrawerAnimator.setDuration(DRAWER_OPEN_DURATION);
                mDrawerAnimator.setInterpolator(new AccelerateInterpolator());

                mDrawerAnimator.start();
            } else {
                mMiddleDrawerProgress = 0.f;
                invalidate();
            }
        }

        private Paint createTextPaint(int defaultInteractiveColor, Typeface typeface) {
            Paint paint = new Paint();
            paint.setColor(defaultInteractiveColor);
            paint.setTypeface(typeface);
            paint.setAntiAlias(true);
            return paint;
        }

        private Bitmap getCircleBitmap(Bitmap bitmap, float outerRadiusPercent) {
            final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                    bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            final Canvas canvas = new Canvas(output);

            final int color = Color.RED;
            final Paint paint = new Paint();
            final Rect rectOuter = new Rect(
                    (int) ((1.f-outerRadiusPercent) / 2.f * bitmap.getWidth()),
                    (int) ((1.f-outerRadiusPercent) / 2.f * bitmap.getHeight()),
                    (int) ((1.f+outerRadiusPercent) / 2.f * bitmap.getWidth()),
                    (int) ((1.f+outerRadiusPercent) / 2.f * bitmap.getHeight()));
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            paint.setAlpha(128);
            canvas.drawOval(new RectF(rectOuter), paint);

            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            bitmap.recycle();

            return output;
        }

        @Override
        public void onDestroy() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            super.onDestroy();
        }

        @Override
        public void onPropertiesChanged(Bundle properties) {
            super.onPropertiesChanged(properties);
            mLowBitAmbient = properties.getBoolean(PROPERTY_LOW_BIT_AMBIENT, false);
            mBurnInProtection = properties.getBoolean(PROPERTY_BURN_IN_PROTECTION, false);
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();
            invalidate();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);
            mAmbient = inAmbientMode;

            updateWatchHandStyle();
            updateStaticBackground();
            updateMinuteBackground();

            /* Check and trigger whether or not timer should be running (only in active mode). */
            updateTimer();
        }

        private void updateWatchHandStyle() {

            Log.d("KinemicWatchFace", "lowbit: " + mLowBitAmbient + ", antiburn: " + mBurnInProtection + ". ambient: " + mAmbient);
            if (mAmbient) {
                mHourPaint.setColor(Color.WHITE);
                mMinutePaint.setColor(Color.WHITE);
                mSecondPaint.setColor(Color.WHITE);
                mTickAndCirclePaint.setColor(Color.WHITE);
                //mRecordingPaint.setColor(mRecordingColor);
                mInnerCirclePaint.setColor(Color.WHITE);
                mBackgroundPaint.setColor(Color.BLACK);

                mHourPaint.setAntiAlias(false);
                mMinutePaint.setAntiAlias(false);
                mSecondPaint.setAntiAlias(false);
                mTickAndCirclePaint.setAntiAlias(false);
                mRecordingPaint.setAntiAlias(false);
                mTextPaint.setAntiAlias(false);
                mInnerCirclePaint.setAntiAlias(false);
                mBackgroundPaint.setAntiAlias(false);

                mHourPaint.clearShadowLayer();
                mMinutePaint.clearShadowLayer();
                mSecondPaint.clearShadowLayer();
                mTickAndCirclePaint.clearShadowLayer();

            } else {
                mHourPaint.setColor(mWatchHandColor);
                mMinutePaint.setColor(mWatchHandColor);
                mSecondPaint.setColor(mWatchHandHighlightColor);
                mTickAndCirclePaint.setColor(mWatchHandColor);
                //mRecordingPaint.setColor(mRecordingColor);
                mInnerCirclePaint.setColor(mWatchHandColor);
                mBackgroundPaint.setColor(mBackgroundColor);

                mHourPaint.setAntiAlias(true);
                mMinutePaint.setAntiAlias(true);
                mSecondPaint.setAntiAlias(true);
                mTickAndCirclePaint.setAntiAlias(true);
                mRecordingPaint.setAntiAlias(true);
                mTextPaint.setAntiAlias(true);
                mInnerCirclePaint.setAntiAlias(true);
                mBackgroundPaint.setAntiAlias(true);
            }
        }

        @Override
        public void onInterruptionFilterChanged(int interruptionFilter) {
            super.onInterruptionFilterChanged(interruptionFilter);
            boolean inMuteMode = (interruptionFilter == WatchFaceService.INTERRUPTION_FILTER_NONE);

            /* Dim display in mute mode. */
            if (mMuteMode != inMuteMode) {
                mMuteMode = inMuteMode;
                mHourPaint.setAlpha(inMuteMode ? 100 : 255);
                mMinutePaint.setAlpha(inMuteMode ? 100 : 255);
                mSecondPaint.setAlpha(inMuteMode ? 80 : 255);
                invalidate();
            }
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);

            /*
             * Find the coordinates of the center point on the screen, and ignore the window
             * insets, so that, on round watches with a "chin", the watch face is centered on the
             * entire screen, not just the usable portion.
             */
            mCenterX = width / 2f;
            mCenterY = height / 2f;

            /*
             * Calculate lengths of different hands based on watch screen size.
             */
            mSecondHandLength = (float) (mCenterX * SECOND_HAND_LENGTH);
            sMinuteHandLength = (float) (mCenterX * MINUTE_HAND_LENGTH);
            sHourHandLength = (float) (mCenterX * HOUR_HAND_LENGTH);

            updateStaticBackground(width, height);
            updateMinuteBackground(width, height);
        }

        /**
         * Captures tap event (and tap type). The {@link WatchFaceService#TAP_TYPE_TAP} case can be
         * used for implementing specific logic to handle the gesture.
         */
        @Override
        public void onTapCommand(int tapType, int x, int y, long eventTime) {
            switch (tapType) {
                case TAP_TYPE_TOUCH:
                    // The user has started touching the screen.
                    break;
                case TAP_TYPE_TOUCH_CANCEL:
                    // The user has started a different gesture or otherwise cancelled the tap.
                    break;
                case TAP_TYPE_TAP:
                    // The user has completed the tap gesture.
                    if (x > 1.5 * mCenterX && y < 0.5 * mCenterY) {
                        // top right corner (K logo)
                        Intent intent = new Intent(getApplicationContext(), RecorderActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        //startService(intent);
                        startActivity(intent);
                    } else if (abs(mCenterX - x) < 30 && abs(mCenterY - y) < 30) {
                        // middle section
                        mDrawerIsOpen = !mDrawerIsOpen;
                        if (mDrawerIsOpen) openMiddleDrawer(true);
                        else {
                            if (mRecorderState == Recorder.STATE_IDLE) {
                                Intent intent = new Intent(getApplicationContext(), RecorderActionActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_START);
                                startActivity(intent);
                            } else if (mRecorderState == Recorder.STATE_STOPPED) {
                                Intent intent = new Intent(getApplicationContext(), RecorderActionActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_SAVE);
                                startActivity(intent);
                            } else if (mRecorderState == Recorder.STATE_RECORDING) {
                                Intent intent = new Intent(getApplicationContext(), RecorderActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            } else {
                                startService(new Intent(getApplicationContext(), RecorderService.class));
                                Intent intent = new Intent(getApplicationContext(), RecorderActionActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_START);
                                startActivity(intent);
                            }
                        }
                    } else {
                        // not mid and not top right corner
                        if (mDrawerIsOpen || mMiddleDrawerProgress > 0.5) {
                            mDrawerIsOpen = false;
                            closeMiddleDrawer(true);
                        }
                    }
                    break;
            }
            invalidate();
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {
            long now = System.currentTimeMillis();
            mCalendar.setTimeInMillis(now);


            final float seconds = (mCalendar.get(Calendar.SECOND) + mCalendar.get(Calendar.MILLISECOND) / 1000f);
            final float secondsRotation = seconds * 6f;
            final float minutesRotation = mCalendar.get(Calendar.MINUTE) * 6f;
            final float hourHandOffset = mCalendar.get(Calendar.MINUTE) / 2f;
            final float hoursRotation = (mCalendar.get(Calendar.HOUR) * 30) + hourHandOffset;

            final int minute = mCalendar.get(Calendar.MINUTE);
            if (minute != mLastMinuteUpdate) {
                updateMinuteBackground();
                mLastMinuteUpdate = minute;
            }

            canvas.drawBitmap(mMinuteBackground, 0, 0 , mBitmapPaint);

            /* dynamic background */
            if (!mAmbient) {
                canvas.save();

                canvas.rotate(secondsRotation, mCenterX, mCenterY);
                canvas.drawBitmap(mGradiantSecondBitmap, 0, 0, mBackgroundPaint);

                canvas.drawCircle(
                        mCenterX,
                        mCenterY,
                        mCenterX*(WATCH_FACE_RADIUS + mMiddleDrawerProgress*(WATCH_FACE_DRAWER_RADIUS-WATCH_FACE_RADIUS)),
                        mBackgroundPaint);

                canvas.restore();
            }

            /* watch hands */
            canvas.save();

            canvas.rotate(hoursRotation, mCenterX, mCenterY);
            canvas.drawLine(
                    mCenterX,
                    mCenterY - CENTER_GAP_AND_CIRCLE_RADIUS,
                    mCenterX,
                    mCenterY - sHourHandLength,
                    mHourPaint);


            canvas.rotate(minutesRotation - hoursRotation, mCenterX, mCenterY);
            canvas.drawLine(
                    mCenterX,
                    mCenterY - CENTER_GAP_AND_CIRCLE_RADIUS,
                    mCenterX,
                    mCenterY - sMinuteHandLength,
                    mMinutePaint);

            /*
             * Ensure the "seconds" hand is drawn only when we are in interactive mode.
             * Otherwise, we only update the watch face once a minute.
             */
            if (!mAmbient) {
                canvas.rotate(secondsRotation - minutesRotation, mCenterX, mCenterY);
                canvas.drawLine(
                        mCenterX,
                        mCenterY - CENTER_GAP_AND_CIRCLE_RADIUS,
                        mCenterX,
                        mCenterY - mSecondHandLength,
                        mSecondPaint);

            }
            canvas.drawCircle(
                    mCenterX,
                    mCenterY,
                    CENTER_GAP_AND_CIRCLE_RADIUS,
                    mTickAndCirclePaint);

            canvas.restore();

            /* drawer */
            if (!mAmbient) {
                canvas.save();

                long passed = mCalendar.getTime().getTime() - mRecordBase;
                final float recordRotation = (passed % 60000) * 0.006f;
                boolean shouldShowRecCircle =(System.currentTimeMillis()/1000 % 2) == 1;

                /* red recording ring */
                if ( mRecording && mMiddleDrawerProgress > 0.95f && shouldShowRecCircle) {
                    canvas.drawCircle(
                            mCenterX,
                            mCenterY,
                            mCenterX*WATCH_FACE_DRAWER_RADIUS*mMiddleDrawerProgress,
                            mRecordingPaint);

                    canvas.rotate(recordRotation, mCenterX, mCenterY);
                    canvas.drawBitmap(mGradiantRecordBitmap, 0, 0, mRecordingPaint);

                    canvas.drawCircle(
                            mCenterX,
                            mCenterY,
                            mCenterX*RECORD_INNER_RADIUS*mMiddleDrawerProgress,
                            mBackgroundPaint);
                } else {
                    /* background for middle area */
                    canvas.drawCircle(
                            mCenterX,
                            mCenterY,
                            mCenterX*WATCH_FACE_DRAWER_RADIUS*mMiddleDrawerProgress,
                            mBackgroundPaint);
                }

                /* white ring around middle area */
                canvas.restore();
                canvas.drawCircle(
                        mCenterX,
                        mCenterY,
                        mCenterX*WATCH_FACE_DRAWER_RADIUS*mMiddleDrawerProgress,
                        mInnerCirclePaint);

                /* drawer content */
                if (mMiddleDrawerProgress > 0.5f) {
                    if (mRecording) {
                        /* recording time */
                        String duration = mDiffFormat.format(new Date(passed - TimeZone.getDefault().getRawOffset()));
                        float w = mTextPaint.measureText(duration);
                        canvas.drawText(duration,
                                mCenterX - w * 0.5f,
                                mMarginText - mTextFontMetrics.ascent,
                                mTextPaint);
                    } else {
                        // maybe some icon to start record?

                        if (mRecorderState == Recorder.STATE_STOPPED) {
                            canvas.drawBitmap(mSaveBitmap, mCenterX - mSaveBitmap.getWidth() * 0.5f, mCenterY - mSaveBitmap.getHeight() * 0.5f, mBitmapPaint);
                        } else {
                            canvas.drawBitmap(mRecordBitmap, mCenterX - mRecordBitmap.getWidth() * 0.5f, mCenterY - mRecordBitmap.getHeight() * 0.5f, mBitmapPaint);
                        }
                    }
                }

                /* red record dot */
                if (mRecording && shouldShowRecCircle && (!mDrawerIsOpen || mMiddleDrawerProgress < 0.9)) {
                    canvas.drawCircle(2.0f * mCenterX - mMarginRight - mIconHeiht * 0.5f, mMarginTop + mIconHeiht * 0.5f, (mIconHeiht * 0.5f + 1.f) * (1.f-mMiddleDrawerProgress), mRecordingPaint);
                }
            } else {
                if (mRecording) {
                    if (mDrawerIsOpen) {
                        canvas.drawCircle(
                                mCenterX,
                                mCenterY,
                                mCenterX * WATCH_FACE_DRAWER_RADIUS,
                                mBackgroundPaint);

                        long passed = mCalendar.getTime().getTime() - mRecordBase;
                        String duration = mDiffFormat.format(new Date(passed - TimeZone.getDefault().getRawOffset()));
                        float w = mTextPaint.measureText(duration);
                        canvas.drawText(duration,
                                mCenterX - w * 0.5f,
                                mMarginText - mTextFontMetrics.ascent,
                                mTextPaint);
                        canvas.drawCircle(
                                mCenterX,
                                mCenterY,
                                mCenterX * WATCH_FACE_DRAWER_RADIUS,
                                mInnerCirclePaint);
                    }

                    /* red record dot */
                    canvas.drawCircle(2.0f * mCenterX - mMarginRight - mIconHeiht * 0.5f, mMarginTop + mIconHeiht * 0.5f, (mIconHeiht * 0.5f + 1.f), mRecordingPaint);
                }
            }

            /* Draw rectangle behind peek card in ambient mode to improve readability. */
            if (mAmbient) {
                canvas.drawRect(mPeekCardBounds, mBackgroundPaint);
            }

            /* Draw every frame as long as we're visible and in interactive mode. */
            if ((isVisible()) && (!mAmbient)) {
                invalidate();
            }
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            if (visible) {
                mGoogleApiClient.connect();
                registerReceiver();
                /* Update time zone in case it changed while we weren't visible. */
                mCalendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            } else {
                unregisterReceiver();
                if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                    Wearable.DataApi.removeListener(mGoogleApiClient, this);
                    mGoogleApiClient.disconnect();
                }
            }

            /* Check and trigger whether or not timer should be running (only in active mode). */
            updateTimer();
        }

        @Override
        public void onPeekCardPositionUpdate(Rect rect) {
            super.onPeekCardPositionUpdate(rect);
            mPeekCardBounds.set(rect);
        }

        private void registerReceiver() {
            bindService(new Intent(getApplicationContext(), RecorderService.class), mRecorderServiceConnection, 0);
            if (!mRegisteredTimeZoneReceiver) {
                mRegisteredTimeZoneReceiver = true;
                IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
                KinemicAnalogWatchFace.this.registerReceiver(mTimeZoneReceiver, filter);
            }
        }

        private void unregisterReceiver() {
            if (mRecorder != null) mRecorder.unregisterOnStateChangeListener(mRecorderStateChangeListener);
            unbindService(mRecorderServiceConnection);
            mRecorderState = -1;

            if (mRegisteredTimeZoneReceiver) {
                mRegisteredTimeZoneReceiver = false;
                KinemicAnalogWatchFace.this.unregisterReceiver(mTimeZoneReceiver);
            }
        }

        /**
         * Starts/stops the {@link #mUpdateTimeHandler} timer based on the state of the watch face.
         */
        private void updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
            }
        }

        /**
         * Returns whether the {@link #mUpdateTimeHandler} timer should be running. The timer
         * should only run in active mode.
         */
        private boolean shouldTimerBeRunning() {
            return isVisible() && !mAmbient;
        }

        /**
         * Handle updating the time periodically in interactive mode.
         */
        private void handleUpdateTimeMessage() {
            invalidate();
            if (shouldTimerBeRunning()) {
                long timeMs = System.currentTimeMillis();
                long delayMs = INTERACTIVE_UPDATE_RATE_MS
                        - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);
            }
        }

        private void updateConfigDataItemAndUiOnStartup() {
            KinemicAnalogWatchFaceUtil.fetchConfigDataMap(mGoogleApiClient,
                    new KinemicAnalogWatchFaceUtil.FetchConfigDataMapCallback() {
                        @Override
                        public void onConfigDataMapFetched(DataMap startupConfig) {
                            // If the DataItem hasn't been created yet or some keys are missing,
                            // use the default values.
                            setDefaultValuesForMissingConfigKeys(startupConfig);
                            KinemicAnalogWatchFaceUtil.putConfigDataItem(mGoogleApiClient, startupConfig, null);

                            updateUiForConfigDataMap(startupConfig);
                        }
                    }
            );
        }

        private void setDefaultValuesForMissingConfigKeys(DataMap config) {
            addIntKeyIfMissing(config, KinemicAnalogWatchFaceUtil.KEY_BACKGROUND_COLOR,
                    KinemicAnalogWatchFaceUtil.COLOR_VALUE_DEFAULT_AND_AMBIENT_BACKGROUND);
            addIntKeyIfMissing(config, KinemicAnalogWatchFaceUtil.KEY_HOURS_COLOR,
                    KinemicAnalogWatchFaceUtil.COLOR_VALUE_DEFAULT_AND_AMBIENT_HOUR_DIGITS);
            addIntKeyIfMissing(config, KinemicAnalogWatchFaceUtil.KEY_MINUTES_COLOR,
                    KinemicAnalogWatchFaceUtil.COLOR_VALUE_DEFAULT_AND_AMBIENT_MINUTE_DIGITS);
            addIntKeyIfMissing(config, KinemicAnalogWatchFaceUtil.KEY_SECONDS_COLOR,
                    KinemicAnalogWatchFaceUtil.COLOR_VALUE_DEFAULT_AND_AMBIENT_SECOND_DIGITS);
        }

        private void addIntKeyIfMissing(DataMap config, String key, int color) {
            if (!config.containsKey(key)) {
                config.putInt(key, color);
            }
        }

        @Override // DataApi.DataListener
        public void onDataChanged(DataEventBuffer dataEvents) {
            for (DataEvent dataEvent : dataEvents) {
                if (dataEvent.getType() != DataEvent.TYPE_CHANGED) {
                    continue;
                }

                DataItem dataItem = dataEvent.getDataItem();
                if (!dataItem.getUri().getPath().equals(
                        KinemicAnalogWatchFaceUtil.PATH_WITH_FEATURE)) {
                    continue;
                }

                DataMapItem dataMapItem = DataMapItem.fromDataItem(dataItem);
                DataMap config = dataMapItem.getDataMap();
                if (Log.isLoggable(TAG, Log.DEBUG)) {
                    Log.d(TAG, "Config DataItem updated:" + config);
                }
                updateUiForConfigDataMap(config);
            }
        }

        private void updateUiForConfigDataMap(final DataMap config) {
            boolean uiUpdated = false;
            for (String configKey : config.keySet()) {
                if (!config.containsKey(configKey)) {
                    continue;
                }
                int color = config.getInt(configKey);
                if (Log.isLoggable(TAG, Log.DEBUG)) {
                    Log.d(TAG, "Found watch face config key: " + configKey + " -> "
                            + Integer.toHexString(color));
                }
                if (updateUiForKey(configKey, color)) {
                    uiUpdated = true;
                }
            }
            if (uiUpdated) {
                invalidate();
            }
        }

        /**
         * Updates the color of a UI item according to the given {@code configKey}. Does nothing if
         * {@code configKey} isn't recognized.
         *
         * @return whether UI has been updated
         */
        private boolean updateUiForKey(String configKey, int color) {
            if (configKey.equals(KinemicAnalogWatchFaceUtil.KEY_BACKGROUND_COLOR)) {
                Log.d("WATCH_UPDATE", "update " + configKey + " to: " + color);
                setInteractiveBackgroundColor(color);
            } else if (configKey.equals(KinemicAnalogWatchFaceUtil.KEY_HOURS_COLOR)) {
                setInteractiveHourDigitsColor(color);
            } else if (configKey.equals(KinemicAnalogWatchFaceUtil.KEY_MINUTES_COLOR)) {
                setInteractiveMinuteDigitsColor(color);
            } else if (configKey.equals(KinemicAnalogWatchFaceUtil.KEY_SECONDS_COLOR)) {
                setInteractiveSecondDigitsColor(color);
            } else {
                Log.w(TAG, "Ignoring unknown config key: " + configKey);
                return false;
            }
            return true;
        }

        @Override  // GoogleApiClient.ConnectionCallbacks
        public void onConnected(Bundle connectionHint) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "onConnected: " + connectionHint);
            }
            Wearable.DataApi.addListener(mGoogleApiClient, Engine.this);
            updateConfigDataItemAndUiOnStartup();
        }

        @Override  // GoogleApiClient.ConnectionCallbacks
        public void onConnectionSuspended(int cause) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "onConnectionSuspended: " + cause);
            }
        }

        @Override  // GoogleApiClient.OnConnectionFailedListener
        public void onConnectionFailed(ConnectionResult result) {
            if (Log.isLoggable(TAG, Log.DEBUG)) {
                Log.d(TAG, "onConnectionFailed: " + result);
            }
        }

        private void updatePaintIfInteractive(Paint paint, int interactiveColor) {
            if (!isInAmbientMode() && paint != null) {
                paint.setColor(interactiveColor);
                updateStaticBackground();
                updateMinuteBackground();
            }
        }

        private void setInteractiveBackgroundColor(int color) {
            mBackgroundColor = color;
            updatePaintIfInteractive(mBackgroundPaint, color);
        }

        private void setInteractiveHourDigitsColor(int color) {
            mWatchHandColor = color;
            updatePaintIfInteractive(mHourPaint, color);
        }

        private void setInteractiveMinuteDigitsColor(int color) {
            mWatchHandColor = color;
            updatePaintIfInteractive(mMinutePaint, color);
        }

        private void setInteractiveSecondDigitsColor(int color) {
            mWatchHandHighlightColor = color;
            updatePaintIfInteractive(mSecondPaint, color);
        }

        private boolean isRecording() {
            return mRecording;
        }

        private ServiceConnection mRecorderServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                Log.d(TAG, "connected to record service");
                mRecorder = ((RecorderService.LocalBinder) service).getRecorder();
                mRecorder.registerOnStateChangeListener(mRecorderStateChangeListener);
                mRecorderStateChangeListener.onRecorderStateChange(mRecorder.getState());
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                Log.d(TAG, "disconnected from record service");
                mRecorder = null;
                mRecorderState = -1;
            }
        };

        private Recorder.OnStateChangeListener mRecorderStateChangeListener = new Recorder.OnStateChangeListener() {
            @Override
            public void onRecorderStateChange(@Recorder.RecorderState int state) {
                mRecorderState = state;
                if (state == Recorder.STATE_RECORDING) {
                    mRecording = true;
                    mRecordBase = mRecorder.getRecordStartTime().getTime();
                } else {
                    mRecording = false;
                }
                invalidate();
            }
        };
    }
}
