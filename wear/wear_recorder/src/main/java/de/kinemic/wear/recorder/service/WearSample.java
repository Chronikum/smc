package de.kinemic.wear.recorder.service;

import android.hardware.SensorEvent;

import java.nio.ByteBuffer;

/**
 * Created by Fabian on 30.11.16.
 */

public class WearSample {

    private static final int length = 24; // 4+8+4+4+4

    public int type;
    public float[] values;
    public long timestamp;

    public WearSample(SensorEvent event) {
        type = event.sensor.getType();
        values = event.values.clone();
        timestamp = event.timestamp;
    }

    private WearSample()  {
        type = 0;
        values = new float[3];
        timestamp = 0L;
    }

    public void writeTo(ByteBuffer byteBuffer) {
        byteBuffer.putInt(type);
        byteBuffer.putLong(timestamp);
        byteBuffer.putFloat(values[0]);
        byteBuffer.putFloat(values[1]);
        byteBuffer.putFloat(values[2]);
    }

    public static WearSample readFrom(ByteBuffer byteBuffer) {
        WearSample sample = new WearSample();
        sample.type = byteBuffer.getInt();
        sample.timestamp = byteBuffer.getLong();
        sample.values[0] = byteBuffer.getFloat();
        sample.values[1] = byteBuffer.getFloat();
        sample.values[2] = byteBuffer.getFloat();
        return sample;
    }

    public byte[] getBytes() {
        ByteBuffer buffer = ByteBuffer.allocate(length);
        writeTo(buffer);
        return buffer.array();
    }

    public static WearSample readFrom(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        return readFrom(buffer);
    }
}
