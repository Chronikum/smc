package de.kinemic.wear.recorder.activities;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.NonNull;

import java.io.File;

import de.kinemic.wear.recorder.service.Recorder;
import de.kinemic.wear.recorder.service.RecorderService;

public class RecorderBaseActivity extends Activity implements Recorder.OnStateChangeListener, Recorder.SaveResultHandler {

    protected Recorder mRecorder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService(new Intent(this, RecorderService.class), mServiceConnection, 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRecorder != null) mRecorder.unregisterOnStateChangeListener(this);
        unbindService(mServiceConnection);
        mRecorder = null;
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mRecorder = ((RecorderService.LocalBinder) service).getRecorder();
            mRecorder.registerOnStateChangeListener(RecorderBaseActivity.this);
            onConnected();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mRecorder = null;
            onDisconnected();
        }
    };

    protected void onConnected() {

    }

    protected void onDisconnected() {

    }

    @Override
    public void onRecorderStateChange(@Recorder.RecorderState int state) {

    }

    @Override
    public void onRecordSaveSuccess(@NonNull File record) {

    }

    @Override
    public void onRecordSaveFailed(@NonNull File record, @NonNull Exception exception) {

    }

    protected void startRecorderService() {
        startService(new Intent(this, RecorderService.class));
    }

    protected void stopRecorderService() {
        stopService(new Intent(this, RecorderService.class));
    }
}
