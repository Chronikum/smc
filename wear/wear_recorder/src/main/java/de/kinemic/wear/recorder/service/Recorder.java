package de.kinemic.wear.recorder.service;

import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.io.File;
import java.lang.annotation.Retention;
import java.util.Date;

import static java.lang.annotation.RetentionPolicy.SOURCE;

/**
 * Created by Fabian on 04.12.16.
 */

public interface Recorder {

    @Retention(SOURCE)
    @IntDef({STATE_IDLE, STATE_RECORDING, STATE_STOPPED})
    @interface RecorderState{}

    /**
     * Nothing recording and nothing in memory.
     */
    int STATE_IDLE = 0;

    /**
     * Recording state.
     */
    int STATE_RECORDING = 1;

    /**
     * Recording stopped and still in memory. The record can be saved or canceled.
     */
    int STATE_STOPPED = 2;

    /**
     * Listener to be notified about state changes of the recorder.
     */
    interface OnStateChangeListener {
        /**
         * State of the recorder has changed.
         * State can be any of {@link Recorder#STATE_IDLE}, {@link Recorder#STATE_RECORDING} or {@link Recorder#STATE_STOPPED}.
         * @param state new state
         */
        void onRecorderStateChange(@RecorderState int state);
    }

    /**
     * Handler to be notified about the success or failing of the {@link Recorder#save()} command
     */
    interface SaveResultHandler {
        void onRecordSaveSuccess(@NonNull File record);
        void onRecordSaveFailed(@NonNull File record, @NonNull Exception exception);
    }

    /**
     * Start a new record.
     * If there was a unsaved record in memory, it will be discarded.
     */
    void start();

    /**
     * Stop the current record. The record will remain in memory and can be saved or discarded.
     */
    void stop();

    /**
     * Stop and discard the current record.
     */
    void cancel();

    /**
     * Discard the current record.
     */
    void discard();

    /**
     * Save the current record. The record will be stopped if it was recording.
     */
    void save();

    /**
     * Save the current record. The record will be stopped if it was recording.
     * The file where the record will be saved will be chosen automatically.
     * @param resultHandler a handler to be notified about save results.
     */
    void save(@Nullable SaveResultHandler resultHandler);

    /**
     * Save the current record. The record will be stopped if it was recording.
     * The file where the record will be saved will be chosen automatically.
     *
     * @param file file where to save the record.
     * @param resultHandler a handler to be notified about save results.
     */
    void save(@NonNull File file, @Nullable SaveResultHandler resultHandler);

    /**
     * Register listener for changes in the recording state
     * @param listener the listener
     */
    void registerOnStateChangeListener(@NonNull OnStateChangeListener listener);

    /**
     * Unregister listener for changes in the recording state
     * @param listener the listener
     */
    void unregisterOnStateChangeListener(@NonNull OnStateChangeListener listener);

    /**
     * Return the current state of the recorder.
     * State can be any of {@link Recorder#STATE_IDLE}, {@link Recorder#STATE_RECORDING} or {@link Recorder#STATE_STOPPED}.
     * @return current state
     */
    @RecorderState int getState();

    /**
     * Return the time when the current record was started.
     * @return the start time
     */
    @Nullable Date getRecordStartTime();
}
