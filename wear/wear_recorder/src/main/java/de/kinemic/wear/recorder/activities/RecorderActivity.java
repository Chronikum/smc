package de.kinemic.wear.recorder.activities;

import android.app.FragmentManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Handler;
import android.os.IBinder;
import android.support.wearable.view.GridPagerAdapter;
import android.support.wearable.view.GridViewPager;
import android.util.Log;

import de.kinemic.wear.recorder.service.Recorder;
import de.kinemic.wear.recorder.service.RecorderService;

/**
 * Created by Fabian on 05.12.16.
 */

public class RecorderActivity extends AbstractGridActivity implements Recorder.OnStateChangeListener {

    GridViewPager mPager;
    private Recorder mRecorder;

    @Override
    protected GridPagerAdapter createGridPageAdapter(Context context, FragmentManager manager) {
        return new RecorderGridPagerAdapter(context, manager);
    }

    @Override
    protected void onPostSetup(final GridViewPager pager) {
        super.onPostSetup(pager);
        mPager = pager;

        mPager.setOnPageChangeListener(new GridViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, int i1, float v, float v1, int i2, int i3) {

            }

            @Override
            public void onPageSelected(int i, int i1) {
                // imitate the 'dismiss by pulling down' interaction, found in notifications
                if (i == 0 && i1 == 0) {
                    finish();
                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        /* part of the 'dismiss by pulling down' workaround */
        (new Handler()).postDelayed(new Runnable() {
            @Override
            public void run() {
                Log.d("Pager", "set current item runnable");
                mPager.setCurrentItem(1, 0);
                mPager.getAdapter().notifyDataSetChanged();
            }
        }, 100L);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bindService(new Intent(this, RecorderService.class), mSericeConnection, 0);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mRecorder != null) mRecorder.unregisterOnStateChangeListener(this);
        unbindService(mSericeConnection);
        mRecorder = null;
        ((RecorderGridPagerAdapter) mPager.getAdapter()).setState(RecorderGridPagerAdapter.STATE_NOT_CONNECTED);
    }

    private ServiceConnection mSericeConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mRecorder = ((RecorderService.LocalBinder) service).getRecorder();
            mRecorder.registerOnStateChangeListener(RecorderActivity.this);
            ((RecorderGridPagerAdapter) mPager.getAdapter()).setState(mRecorder.getState());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            mRecorder = null;
            ((RecorderGridPagerAdapter) mPager.getAdapter()).setState(RecorderGridPagerAdapter.STATE_NOT_CONNECTED);
        }
    };

    @Override
    public void onRecorderStateChange(@Recorder.RecorderState int state) {
        ((RecorderGridPagerAdapter) mPager.getAdapter()).setState(state);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        finish();
    }
}
