package de.kinemic.wear.recorder.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.wearable.activity.ConfirmationActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import de.kinemic.wear.recorder.activities.RecorderNotificationActivity;
import de.kinemic.wear.recorder.R;
import de.kinemic.wear.recorder.activities.RecorderActionActivity;


/**
 * Created by Fabian on 04.12.16.
 */

public class RecorderService extends Service implements Recorder.OnStateChangeListener {

    private final DateFormat mDateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());

    private static final int NOTIFICATION_ID = 153373;

    private final LocalBinder mBinder = new LocalBinder();
    private InternalSensorRecorder mRecorder;
    private NotificationManager mNotificationManager;
    private boolean mForeground = false;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_NOT_STICKY;
    }

    public void onCreate() {
        super.onCreate();

        mRecorder = new InternalSensorRecorder(getApplicationContext());
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        onRecorderStateChange(mRecorder.getState());
        mRecorder.registerOnStateChangeListener(this);
    }

    public void onDestroy() {
        super.onDestroy();

        mRecorder.quit();
        mRecorder.unregisterOnStateChangeListener(this);

        if (mNotificationManager != null) mNotificationManager.cancel(NOTIFICATION_ID);
        if (mForeground) stopForeground(true);
    }

    @Override
    public void onRecorderStateChange(@Recorder.RecorderState int state) {
        if (mForeground) stopForeground(true);
        if (mNotificationManager != null) mNotificationManager.cancel(NOTIFICATION_ID);

        Notification notification = createNotification(state);
        if (state == Recorder.STATE_RECORDING) {
            startForeground(NOTIFICATION_ID, notification);
            mForeground = true;
        } else if (state == Recorder.STATE_STOPPED) {
            mNotificationManager.notify(NOTIFICATION_ID, notification);
        } else if (state == Recorder.STATE_IDLE) {
            //mNotificationManager.notify(NOTIFICATION_ID, notification);
        }
    }

    public class LocalBinder extends Binder {
        public Recorder getRecorder() {
            return mRecorder;
        }
    }

    private Notification createNotification(@Recorder.RecorderState int state) {
        Intent startIntent = new Intent(this, RecorderActionActivity.class);
        //Intent startIntent = new Intent(this, ConfirmationActivity.class);
        startIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION );
        startIntent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_START);
        startIntent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.SUCCESS_ANIMATION);
        PendingIntent startPendingIntent = PendingIntent.getActivity(this, 0, startIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action startAction = new Notification.Action.Builder(R.drawable.ic_full_record_rec_white, "Start", startPendingIntent).build();

        Intent stoptIntent = new Intent(this, RecorderActionActivity.class);
        stoptIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        stoptIntent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_STOP);
        PendingIntent stopPendingIntent = PendingIntent.getActivity(this, 1, stoptIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action stopAction = new Notification.Action.Builder(R.drawable.ic_full_stop_white, "Stop", stopPendingIntent).build();

        Intent discardIntent = new Intent(this, RecorderActionActivity.class);
        discardIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        discardIntent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_DISCARD);
        PendingIntent discardPendingIntent = PendingIntent.getActivity(this, 2, discardIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action discardAction = new Notification.Action.Builder(R.drawable.ic_full_cancel_white, "Discard", discardPendingIntent).build();

        Intent cancelIntent = new Intent(this, RecorderActionActivity.class);
        cancelIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        cancelIntent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_CANCEL);
        PendingIntent cancelPendingIntent = PendingIntent.getActivity(this, 3, cancelIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action cancelAction = new Notification.Action.Builder(R.drawable.ic_full_cancel_white, "Cancel", cancelPendingIntent).build();

        Intent savetIntent = new Intent(this, RecorderActionActivity.class);
        savetIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        savetIntent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_SAVE);
        PendingIntent savePendingIntent = PendingIntent.getActivity(this, 4, savetIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        Notification.Action saveAction = new Notification.Action.Builder(R.drawable.ic_full_save_white, "Save", savePendingIntent).build();

        Notification.Action saveOnStopAction = new Notification.Action.Builder(R.drawable.ic_full_stop_white, "Stop", savePendingIntent).build();

        Intent displayIntent = new Intent(this, RecorderNotificationActivity.class);
        PendingIntent displayPendingIntent = PendingIntent.getActivity(this, 5, displayIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        ArrayList<Notification.Action> actions = new ArrayList<>();

        String title = "";
        String text = "Swipe right for actions";
        boolean ongoing = false;
        switch (state) {
            case Recorder.STATE_IDLE:
                actions.add(startAction);
                title = "Recorder";
                ongoing = false;
                break;
            case Recorder.STATE_RECORDING:
                actions.add(saveOnStopAction);
                //actions.add(stopAction);
                //actions.add(saveAction);
                //actions.add(cancelAction);
                title = "Recording";
                text = "Started: " + mDateFormat.format(mRecorder.getRecordStartTime());
                ongoing = true;
                break;
            case Recorder.STATE_STOPPED:
                actions.add(saveAction);
                //actions.add(discardAction);
                title = "Recorder stopped";
                ongoing = true;
                break;
        }


        Notification.WearableExtender extender = new Notification.WearableExtender()
                .addActions(actions)
                .setDisplayIntent(displayPendingIntent)
                .setCustomSizePreset(Notification.WearableExtender.SIZE_SMALL)
                .setBackground(BitmapFactory.decodeResource(getResources(), R.drawable.background1));

        Notification.Builder builder = new Notification.Builder(getApplicationContext())
                .setContentTitle(title)
                .setSmallIcon(R.drawable.kinemic_icon_color)
                .extend(extender)
                .setOngoing(ongoing);
        return builder.build();
    }
}
