/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.kinemic.wear.recorder.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.support.wearable.activity.ConfirmationActivity;
import android.support.wearable.view.CardFragment;
import android.support.wearable.view.FragmentGridPagerAdapter;
import android.support.wearable.view.GridPagerAdapter;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import de.kinemic.wear.recorder.R;
import de.kinemic.wear.recorder.fragments.ActionFragment;
import de.kinemic.wear.recorder.fragments.RecorderStateCardFragment;
import de.kinemic.wear.recorder.fragments.TransparentFinishFragment;
import de.kinemic.wear.recorder.service.Recorder;
import de.kinemic.wear.recorder.service.RecorderService;

/**
 * Constructs fragments as requested by the GridViewPager. For each row a different background is
 * provided.
 * <p>
 * Always avoid loading resources from the main thread. In this sample, the background images are
 * loaded from an background task and then updated using {@link #notifyRowBackgroundChanged(int)}
 * and {@link #notifyPageBackgroundChanged(int, int)}.
 */
public class RecorderGridPagerAdapter extends FragmentGridPagerAdapter {
    private static final int TRANSITION_DURATION_MILLIS = 100;

    public static final int STATE_NOT_CONNECTED = 3;

    private final Context mContext;
    private List<Row>[] mRows;
    private ColorDrawable mDefaultBg;
    private int mState = STATE_NOT_CONNECTED;

    private ColorDrawable mClearBg;

    public RecorderGridPagerAdapter(Context ctx, FragmentManager fm) {
        super(fm);
        mContext = ctx;

        mRows = new List[4];
        for (int i = 0; i < 4; ++i) {
            mRows[i] = createRowsForState(i);
        }

        mDefaultBg = new ColorDrawable(ctx.getResources().getColor(R.color.kinemic_gray));
        mClearBg = new ColorDrawable(ctx.getResources().getColor(android.R.color.transparent));
    }

    public void setState(int state) {
        mState = state;
        notifyDataSetChanged();
    }

    private List<Row> createRowsForState(int state) {
        List<Row> rows = new ArrayList<Row>();

        rows.add(new Row( TransparentFinishFragment.create() ));

        switch (state) {

            case STATE_NOT_CONNECTED:
                rows.add(
                    new Row(
                        RecorderStateCardFragment.create(),
                        ActionFragment.create(R.drawable.ic_full_record_rec_white, R.string.start_record, mStartListener)
                ));
                break;
            case Recorder.STATE_IDLE:
                rows.add(
                    new Row(
                        RecorderStateCardFragment.create(),
                        ActionFragment.create(R.drawable.ic_full_record_rec_white, R.string.start_record, mStartListener),
                        ActionFragment.create(R.drawable.ic_full_clear_white, R.string.stop_recorder, mStopRecorderListener)
                ));
                break;
            case Recorder.STATE_RECORDING:
                rows.add(
                    new Row(
                        RecorderStateCardFragment.create(),
                        ActionFragment.create(R.drawable.ic_full_stop_white, R.string.stop_record, mSaveListener) // auto save on stop
                        //ActionFragment.create(R.drawable.ic_full_save_white, R.string.save_record, mSaveListener)
                        //ActionFragment.create(R.drawable.ic_full_cancel_white, R.string.cancel_record, mCancelListener)
                ));
                break;
            case Recorder.STATE_STOPPED:
                rows.add(
                    new Row(
                        RecorderStateCardFragment.create(),
                        ActionFragment.create(R.drawable.ic_full_save_white, R.string.save_record, mSaveListener)
                        // ActionFragment.create(R.drawable.ic_full_cancel_white, R.string.discard_record, mDiscardListener),
                        // ActionFragment.create(R.drawable.ic_full_clear_white, R.string.stop_recorder, mStopRecorderListener)
                ));
                break;
        }

        return rows;
    }


    private ActionFragment.Listener mStartListener = new ActionFragment.Listener() {
        @Override
        public void onActionPerformed() {
            Log.d("GridPager", "start!");
            mContext.startService(new Intent(mContext, RecorderService.class));

            Intent startIntent = new Intent(mContext, RecorderActionActivity.class);
            startIntent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_START);
            startIntent.putExtra(ConfirmationActivity.EXTRA_ANIMATION_TYPE, ConfirmationActivity.SUCCESS_ANIMATION);
            ((Activity) mContext).startActivityForResult(startIntent, RecorderActionActivity.ACTION_START);
        }
    };

    private ActionFragment.Listener mStopListener = new ActionFragment.Listener() {
        @Override
        public void onActionPerformed() {
            Log.d("GridPager", "stop!");
            Intent stoptIntent = new Intent(mContext, RecorderActionActivity.class);
            stoptIntent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_STOP);
            ((Activity) mContext).startActivityForResult(stoptIntent, RecorderActionActivity.ACTION_STOP);
        }
    };

    private ActionFragment.Listener mDiscardListener = new ActionFragment.Listener() {
        @Override
        public void onActionPerformed() {
            Log.d("GridPager", "discard!");
            Intent discardIntent = new Intent(mContext, RecorderActionActivity.class);
            discardIntent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_DISCARD);
            ((Activity) mContext).startActivityForResult(discardIntent, RecorderActionActivity.ACTION_DISCARD);
        }
    };

    private ActionFragment.Listener mSaveListener = new ActionFragment.Listener() {
        @Override
        public void onActionPerformed() {
            Log.d("GridPager", "save!");
            Intent savetIntent = new Intent(mContext, RecorderActionActivity.class);
            savetIntent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_SAVE);
            ((Activity) mContext).startActivityForResult(savetIntent, RecorderActionActivity.ACTION_SAVE);
        }
    };

    private ActionFragment.Listener mStopRecorderListener = new ActionFragment.Listener() {
        @Override
        public void onActionPerformed() {
            Log.d("GridPager", "stop service!");
            mContext.stopService(new Intent(mContext, RecorderService.class));
            ((Activity) mContext).finish();
        }
    };

    private ActionFragment.Listener mCancelListener = new ActionFragment.Listener() {
        @Override
        public void onActionPerformed() {
            Log.d("GridPager", "cancel!");
            Intent cancelIntent = new Intent(mContext, RecorderActionActivity.class);
            cancelIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            cancelIntent.putExtra(RecorderActionActivity.EXTRA_ACTION, RecorderActionActivity.ACTION_CANCEL);
            ((Activity) mContext).startActivityForResult(cancelIntent, RecorderActionActivity.ACTION_CANCEL);
        }
    };

    LruCache<Integer, Drawable> mRowBackgrounds = new LruCache<Integer, Drawable>(3) {
        @Override
        protected Drawable create(final Integer row) {
            int resid = BG_IMAGES[row % BG_IMAGES.length];

            //if (row == 0) return mClearBg;

            new DrawableLoadingTask(mContext) {
                @Override
                protected void onPostExecute(Drawable result) {
                    TransitionDrawable background = new TransitionDrawable(new Drawable[] {
                            mDefaultBg,
                            result
                    });
                    mRowBackgrounds.put(row, background);
                    notifyRowBackgroundChanged(row);
                    background.startTransition(TRANSITION_DURATION_MILLIS);
                }
            }.execute(resid);
            return mDefaultBg;
        }
    };

    LruCache<Point, Drawable> mPageBackgrounds = new LruCache<Point, Drawable>(3) {
        @Override
        protected Drawable create(final Point page) {
            // place bugdroid as the background at row 2, column 1
            /*if (false) {
                int resid = any;
                new DrawableLoadingTask(mContext) {
                    @Override
                    protected void onPostExecute(Drawable result) {
                        TransitionDrawable background = new TransitionDrawable(new Drawable[] {
                                mClearBg,
                                result
                        });
                        mPageBackgrounds.put(page, background);
                        notifyPageBackgroundChanged(page.y, page.x);
                        background.startTransition(TRANSITION_DURATION_MILLIS);
                    }
                }.execute(resid);
            }*/
            return GridPagerAdapter.BACKGROUND_NONE;
        }
    };

    private Fragment cardFragment(int titleRes, int textRes) {
        Resources res = mContext.getResources();
        CardFragment fragment =
                CardFragment.create(res.getText(titleRes), res.getText(textRes));
        // Add some extra bottom margin to leave room for the page indicator
        fragment.setCardMarginBottom(
                res.getDimensionPixelSize(R.dimen.card_margin_bottom));
        return fragment;
    }

    static final int[] BG_IMAGES = new int[] {
            R.drawable.background1,
    };

    /** A convenient container for a row of fragments. */
    private class Row {
        final List<Fragment> columns = new ArrayList<Fragment>();

        public Row(Fragment... fragments) {
            for (Fragment f : fragments) {
                add(f);
            }
        }

        public void add(Fragment f) {
            columns.add(f);
        }

        Fragment getColumn(int i) {
            return columns.get(i);
        }

        public int getColumnCount() {
            return columns.size();
        }
    }

    @Override
    public Fragment getFragment(int row, int col) {
        Row adapterRow = mRows[mState].get(row);
        return adapterRow.getColumn(col);
    }

    @Override
    public Drawable getBackgroundForRow(final int row) {
        return mRowBackgrounds.get(row);
    }

    @Override
    public Drawable getBackgroundForPage(final int row, final int column) {
        return mPageBackgrounds.get(new Point(column, row));
    }

    @Override
    public int getRowCount() {
        return mRows[mState].size();
    }

    @Override
    public int getColumnCount(int rowNum) {
        return mRows[mState].get(rowNum).getColumnCount();
    }

    class DrawableLoadingTask extends AsyncTask<Integer, Void, Drawable> {
        private static final String TAG = "Loader";
        private Context context;

        DrawableLoadingTask(Context context) {
            this.context = context;
        }

        @Override
        protected Drawable doInBackground(Integer... params) {
            Log.d(TAG, "Loading asset 0x" + Integer.toHexString(params[0]));
            return context.getResources().getDrawable(params[0]);
        }
    }
}
