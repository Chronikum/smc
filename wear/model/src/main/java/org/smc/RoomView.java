package org.smc;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.View;

import org.smc.model.Device;
import org.smc.model.Room;

/**
 * Created by Fabian on 28.10.17.
 */

public class RoomView extends View {

    private final Room mRoom;
    private final Paint mRoomPaint;
    private final Paint mBitmapPaint;
    private int mWidth, mHeight;
    private Bitmap mBitmap;
    private Canvas mCanvas;
    private Path mRoomPath;
    private Paint mDevicePaint;
    private Paint mDeviceSelectedPaint;
    private int mDeviceSelectedID = -1;

    public RoomView(Context context, Room room) {
        super(context);
        mRoom = room;
        mRoomPaint = new Paint();
        mRoomPaint.setAntiAlias(true);
        mRoomPaint.setDither(true);
        mRoomPaint.setColor(Color.GREEN);
        mRoomPaint.setStyle(Paint.Style.STROKE);
        mRoomPaint.setStrokeJoin(Paint.Join.ROUND);
        mRoomPaint.setStrokeCap(Paint.Cap.ROUND);
        mRoomPaint.setStrokeWidth(8);
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);

        mDevicePaint = new Paint();
        mDevicePaint.setAntiAlias(true);
        mDevicePaint.setDither(true);
        mDevicePaint.setColor(Color.BLUE);
        mDevicePaint.setStyle(Paint.Style.STROKE);
        mDevicePaint.setStrokeJoin(Paint.Join.ROUND);
        mDevicePaint.setStrokeCap(Paint.Cap.ROUND);
        mDevicePaint.setStrokeWidth(4);

        mDeviceSelectedPaint = new Paint();
        mDeviceSelectedPaint.setAntiAlias(true);
        mDeviceSelectedPaint.setDither(true);
        mDeviceSelectedPaint.setColor(Color.RED);
        mDeviceSelectedPaint.setStyle(Paint.Style.STROKE);
        mDeviceSelectedPaint.setStrokeJoin(Paint.Join.ROUND);
        mDeviceSelectedPaint.setStrokeCap(Paint.Cap.ROUND);
        mDeviceSelectedPaint.setStrokeWidth(6);

        mRoomPath = new Path();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawBitmap( mBitmap, 0, 0, mBitmapPaint);

        if (mDeviceSelectedID != -1) {
            Device selected = mRoom.gadgets.get(mDeviceSelectedID);
            canvas.drawCircle((float) selected.position.x * mWidth, (float) selected.position.y * mHeight, 10, mDeviceSelectedPaint);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);

        mRoomPath.reset();
        mRoomPath.moveTo((float) mRoom.path.get(mRoom.path.size()-1).x * w, (float) mRoom.path.get(mRoom.path.size()-1).y * h);
        for (int i = 0; i < mRoom.path.size(); ++i) {
            mRoomPath.lineTo((float) mRoom.path.get(i).x * w, (float) mRoom.path.get(i).y * h);
        }
        mCanvas.drawPath(mRoomPath,  mRoomPaint);

        for (int i = 0; i < mRoom.gadgets.size(); ++i) {
            Device device = mRoom.gadgets.get(i);
            mCanvas.drawCircle((float) device.position.x * mWidth, (float) device.position.y * mHeight, 10, mDevicePaint);
        }

        invalidate();
    }

    public void selectDevice(int index) {
        mDeviceSelectedID = index;
        invalidate();
    }
}
