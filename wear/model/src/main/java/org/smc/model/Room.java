package org.smc.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Fabian on 28.10.17.
 */

public class Room {
    public final List<Point> path;
    public final String name;
    public final List<Device> gadgets;

    public Room(String name, List<Point> path, List<Device> gadget) {
        this.name = name;
        this.path = path;
        this.gadgets = gadget;
    }

    public static Room fromJson(JSONObject json) throws JSONException {
        /* {'name': 'room1', 'path':[{'x':0.1, 'y':0.2}], 'devices':[{...}]} */
        final String name = json.getString("name");
        JSONArray points = json.getJSONArray("path");
        ArrayList<Point> point_list = new ArrayList<>();
        for (int i = 0; i < points.length(); ++i) {
            point_list.add(Point.fromJson(points.getJSONObject(i)));
        }
        JSONArray devices = json.getJSONArray("devices");
        ArrayList<Device> device_list = new ArrayList<>();
        for (int i = 0; i < devices.length(); ++i) {
            device_list.add(Device.fromJson(devices.getJSONObject(i)));
        }
        return new Room(name, Collections.unmodifiableList(point_list), Collections.unmodifiableList(device_list));
    }

    public static final JSONObject MOCK;
    static {
        JSONObject tmp;
        try {
            tmp = new JSONObject(
                    "{'name': 'Room-name', 'path':[{'x':0.1, 'y':0.1}, {'x': 0.1, 'y': 0.9}, {'x':0.9, 'y': 0.9}, {'x':0.9, 'y':0.1}], 'devices':[{'id': 1, 'name': 'Dose 1', 'position': {'x': 0.1, 'y': 0.5}, 'url': null, 'type': 'outlet'}, {'id': 2, 'name': 'Dose 2', 'position': {'x': 0.1, 'y': 0.7}, 'url': null, 'type': 'outlet'}]}}"
            );
        } catch (JSONException e) {
            e.printStackTrace();
            tmp = null;
        }
        MOCK = tmp;
    }

}
