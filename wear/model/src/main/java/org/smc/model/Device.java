package org.smc.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Fabian on 28.10.17.
 */

public class Device {
    public final String name;
    public final int id;
    public final Point position;
    public final String pictureUrl;
    public final String type;

    public Device(String name, int id, Point position, String pictureUrl, String type) {
        this.name = name;
        this.id = id;
        this.position = position;
        this.pictureUrl = pictureUrl;
        this.type = type;
    }

    public static Device fromJson(JSONObject json) throws JSONException {
        /* {'id': 1, 'name': 'Lampe 1', 'position': {...}, 'url': 'http://...', 'type': 'light'} */
        return new Device(
                json.getString("name"),
                json.getInt("id"),
                Point.fromJson(json.getJSONObject("position")),
                json.has("url") && !json.isNull("url") ? json.getString("url") : null,
                json.getString("type")
        );
    }
}
