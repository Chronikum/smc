package org.smc.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Fabian on 28.10.17.
 */

public class Point {
    public final double x;
    public final double y;

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public static Point fromJson(JSONObject json) throws JSONException {
        /* {'x': 0.1, 'y': 0.4} */
        return new Point(json.getDouble("x"), json.getDouble("y"));
    }
}
